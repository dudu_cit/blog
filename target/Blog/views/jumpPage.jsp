<%--
  Created by IntelliJ IDEA.
  User: dudu
  Date: 2021/3/20
  Time: 14:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"  isELIgnored="false" %>
<html>
<head>
    <meta charset="utf-8">
    <title>CIT博客</title>
    <meta name="keywords" content="CIT博客" />
    <meta name="description" content="CIT博客" />
    <link href="../css/base.css" rel="stylesheet">
    <link href="../css/index.css" rel="stylesheet">
    <link href="../css/m.css" rel="stylesheet">
    <script src="../js/jquery-2.1.1.min.js"></script>

    <style>
        body {
            position: relative;
            background-color: #eeeeee;
        }

        body .jump-page {
            position: absolute;
            background: #d5f2e8;
            width: 500px;
            height: 400px;
            left: 35%;
            top: 22%;
            border:1px solid #dddddd;
            border-radius:5px;
            box-shadow: 1px 1px 3px #ececec;
        }
        .jumpPage-head{
            margin-top: 20px;
            margin-left: 20px;
            margin-bottom: 20px;
            font-size: 24px;
            font-weight: bold;
            color: #00a0ff;
        }
        .jumpPage-line{
            width: 95%;
            height: 1px;
            margin-left: 10px;
            background-color: #db5d05;
        }
        .jumpPage-body{
            margin-top: 50px;
            margin-left: 20px;
            margin-bottom: 100px;
            padding-left: 50px;
            font-size: 16px;
            color: #000;
        }
        .jumpPage-footer{
            margin-top: 50px;
            margin-left: 20px;
            padding: 10px 10px 10px 10px;

        }

        .jumpPage-footer-btn1{
            margin-left: 100px;
            font-weight: bold;
            font-size: 16px;
        }
        .jumpPage-footer-btn2{
            margin-left: 50px;
            font-weight: bold;
            font-size: 16px;
        }
        .jumpPage-footer-btn1:hover,.jumpPage-footer-btn2:hover{
            color: #00a0ff;
        }
    </style>
</head>
<body>

<div class="jump-page">
    <div class="jumpPage-head">
        <h4 class="modal-title">
            CIT 博客提醒您
        </h4>
    </div>
    <div class="jumpPage-line"></div>
    <div class="jumpPage-body">
        <p>恭喜您 ${session.username} , 文章审核成功！</p>
    </div>
    <div class="jumpPage-line"></div>
    <div class="jumpPage-footer">
        <a href="/user/markdown" class="jumpPage-footer-btn1">返回创作中心</a>
        <a href="#" class="jumpPage-footer-btn2"  id="jumpPage-footer-btn2">去看看吧</a>
    </div>
</div>

<script>
    function getQueryVariable(variable)
    {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            if(pair[0] == variable){return pair[1];}
        }
        return(false);
    }

    /** 查看 发布的文章 **/
    $(function () {
        $("#jumpPage-footer-btn2").click(function () {
            //当前文章的 id
            var aid = getQueryVariable("aid");
            //alert("aid : " +aid);
            //将文章的id 追加到请求的后面
            window.location.href = "/article/blog_content?aid="+aid;
        });
    });
</script>
</body>
</html>
