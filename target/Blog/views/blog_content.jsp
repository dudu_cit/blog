<%@ page language="java" contentType="text/html;iso-8859-1" import="java.util.*" 
    pageEncoding="utf-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>CIT博客</title>
    <meta name="keywords" content="CIT博客" />
    <meta name="description" content="CIT博客" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/base.css" rel="stylesheet">
    <link href="../css/index.css" rel="stylesheet">
    <link href="../css/m.css" rel="stylesheet">
    <link href="../css/model.css" rel="stylesheet">
    <link rel="stylesheet" href="../fonts/iconfont.css">
    <script src="../js/jquery-2.1.1.min.js"></script>
    <script src="https://cdn.bootcss.com/showdown/1.3.0/showdown.min.js"></script>  <!-- 转化 js -->
    <link rel="stylesheet" href="../layui/css/layui.css">
    <link rel="stylesheet" href="https://cdn.bootcss.com/github-markdown-css/2.10.0/github-markdown.min.css">
    <script src="../layui/layui.js" charset="utf-8"></script>
    <!-- 转化后的 css样式 -->
    <!--[if lt IE 9]>
    <script src="js/modernizr.js"></script>
    <![endif]-->
    <script>

        //判断session 是否存在

        var session = "${session.id}";

        // alert(session);
        $(function () {
            if(session.length ===0 ){
                //window.location.href = "/user/login";
                document.getElementById("login_after").style.display="none";
                document.getElementById("login_after2").style.display="none";
                //alert("未登录用户");
            }else{
                document.getElementById("login_before").style.display="none";
                document.getElementById("login_after_name").innerHTML = "${session.username}";
                //alert("已经登陆"+"${session.username}");
            }
        });

        window.onload = function ()
        {
            var oH2 = document.getElementsByTagName("h2")[0];
            var oUl = document.getElementsByTagName("ul")[0];
            oH2.onclick = function ()
            {
                var style = oUl.style;
                style.display = style.display == "block" ? "none" : "block";
                oH2.className = style.display == "block" ? "open" : ""
            }
        }
    </script>
    <style>
        .news_infos-head{
            background-color: #faf5f8;
            padding-left: 10px;
            padding-top: 10px;
            border-radius: 20px;
        }

        .bs-example {
            background-color: white;
            padding: 10px 25px;
            color: black;
            border: 2px solid #4CAF50;
        }

        .news_infos-content{
            margin-top: 10px;
        }

        .article-content-body{
            width: 700px;
            height: 500px;
            margin-left: 30px;

            border:1px solid #dddddd;
            border-radius:15px;
            box-shadow: 1px 1px 3px #ececec;
        }


    </style>
</head>
<body>
<header>
  <div class="tophead">
    <div class="logo"><a href="/">CIT 博客</a></div>
    <nav class="topnav" id="topnav">
      <ul>
          <li><a href="/admin/index">首页</a></li>
          <li><a href="/admin/blog">博客</a></li>
          <li><a href="/user/collection">收藏</a></li>
          <li><a href="/user/info">动态</a></li>
          <li><a href="/user/messages">消息</a></li>
          <li><a href="/user/markdown">创作</a></li>
          <li><a href="/my_blog">我的</a></li>
          <li id="login_before"><a href="/user/login">登陆/注册</a></li>
          <li id="login_after"><a href="/user/showUserBaseInfo" id="login_after_name">DUDU</a></li>
          <li id="login_after2"><a href="#modal-one" class="out_btn-big">退出登陆</a></li>
      </ul>
    </nav>
  </div>
</header>


<!-- 模态框（Modal） -->
<div class="modal" id="modal-one" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-header">
            <h2>CIT博客提醒您</h2>
            <a href="#" class="out_btn-close" aria-hidden="true">×</a>
        </div>
        <div class="modal-body">
            <p>是否确认退出？一旦退出就需要重新登陆</p>
        </div>
        <div class="modal-footer">
            <a href="/user/outLogin" class="out_btn">确 定</a>
        </div>
    </div>
</div>

<article>
    <h1 class="t_nav"><span>像“草根”一样，紧贴着地面，低调的存在，冬去春来，枯荣无恙。</span><a href="/" class="n1">首页</a><a href="/" class="n2">博客</a></h1>
    <div class="ab_box">
        <div class="leftbox">
            <div class="newsview">
                <div class="news_infos">
                    <div class="news_infos-head">
                        <h2>${articleModel_content.article_title}</h2>
                        <div class="autor">
                            <!-- 文章 类型 和 内容 js 填充 -->
                            <span class="lm"><a href="/" title="CSS3|Html5" target="_blank" class="classname">${articleModel_content.article_type_name} | <a class="classname" id="article_status">${articleModel_content.article_status_name}</a></a></span>
                            <span class="dtime">${articleModel_content.article_create_time}</span>
                            <span class="viewnum">浏览（${articleModel_content.article_view_count}）</span>
                            <c:set var="id" scope="session" value="${session.id}"/>
                            <c:if test="${id != null }">
                                <c:set var="isCollect" scope="session" value="${isCollect}"/>
                                <c:set var="isLike" scope="session" value="${isLike}"/>
                                <c:if test="${isLike == 0  }">
                                    <span onclick="like()"><i class="layui-icon layui-icon-heart" style="padding-right:5px;"></i>点赞</span>
                                </c:if>
                                <c:if test="${isLike == 1 }">
                                    <span onclick="cancelLike()"><i class="layui-icon layui-icon-heart-fill" style="padding-right:5px;color:#c03a38"></i>已点赞</span>
                                </c:if>
                                <c:if test="${isCollect == 0  }">
                                    <span onclick="collete('添加收藏','CJia_add?aid=${articleModel_content.article_id}')"><i class="layui-icon layui-icon-star" style="padding-right:5px;"></i>收藏</span>
                                </c:if>
                                <c:if test="${isCollect == 1 }">
                                    <span onclick="cancelCollect()"><i class="layui-icon layui-icon-star-fill" style="padding-right:5px;color:#c03a38"></i>已收藏</span>
                                </c:if>
                            </c:if>
                            <c:if test="${id == null }">
                                <span onclick="toLogin()"><i class="layui-icon layui-icon-heart" style="padding-right:5px;"></i>点赞</span>
                                <span onclick="toLogin()"><i class="layui-icon layui-icon-star" style="padding-right:5px;"></i>收藏</span>
                            </c:if>
                            <span>发布作者：<a href="#">${authorInfoModel.author_name}</a></span>
                        </div>
                    </div>
                    <div class="news_infos-content">
                        <!--- 这里是文章显示区域 -->
                        <div   v-html="blog" id="article-content-body" class="markdown-body" >
                            ${articleModel_content.article_content}
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="rightbox">
            <div class="aboutme">
                <h2 class="hometitle">关于作者</h2>
                <div class="avatar"> <img src="../images/yy0.png"> </div>
                <div class="ab_con">
                    <a href="#" class="a-style"><p style="font-family: bold">${authorInfoModel.author_name}</p></a>
                    <p>博客： ${authorInfoModel.blog_number}</p>
                    <p>排名： ${authorInfoModel.author_order}</p>
                    <p>点赞： ${authorInfoModel.like_number}</p>
                    <p>访问： ${authorInfoModel.access_number}</p>
                    <p>粉丝： ${authorInfoModel.fax_number}</p>
                    <div>
                        <a href="#"><button class="bs-example">私信</button></a>
                        <a href="#"><button class="bs-example">关注</button></a>
                    </div>
                </div>
            </div>
            <div class="weixin">
                <h2 class="hometitle">微信关注</h2>
                <ul>
                    <img src="../images/wx.jpg">
                </ul>
            </div>
        </div>
    </div>
</article>

<footer>
    <p>@DUDU <a href="/">CIT博客</a> <a href="/">蜀ICP备11002373号-1</a></p>
</footer>
<script src="../js/nav.js"></script>
<script type="text/javascript">

    /** 判断 文章的类型，1：原创，0：转载 **/
    function getArticleStatus(status) {
        if(status === "1" ) return "原创";
        else return "转载";
    }

    /** 显示文章：原创  or 转载 **/
    var status = "${articleModel_content.article_status}"+"";
    //alert("status : " +status);
    var article_status = getArticleStatus(status);
    //alert("article_status: "+article_status);
    document.getElementById("article_status").innerHTML = article_status+"";


    /** 错误提示 */
    function error() {
        layui.use('layer', function() {
            var layer = layui.layer;
            layer.msg('请求失败');
        });
    }

    /** 提示用户是登陆**/
    function toLogin() {
        layui.use('layer', function() {
            var layer = layui.layer;
            layer.msg('请先登陆？', {
                time: 0 //不自动关闭
                ,btn: ['确定', '取消']
                ,yes: function(index){
                    window.location.href = "/user/login";
                }
            });
        });
    }

    /** 得到用户 id,文章id,点赞得个数  **/
    var uid = "${session.id}";
    var aid = "${articleModel_content.article_id}";
    var likeCount = "${articleModel_content.article_like_count}";

    /** 点赞 **/
    function like() {
        //alert("uid :" +uid+" aid : "+aid);
        $.ajax({
            url:"/article/addLike",
            data:{like_user_id:uid,like_article_id:aid,article_like_count:likeCount},
            type:"post",
            dataType:"json",
            success:function (data) {
                if(data.code === 1){
                    location.reload();
                }else{error();}
            },
            error: function (xhr, textStatus, errorThrown) {
                window.location.href = "error";
            }
        });
    }

    /** 取消点赞 **/
    function cancelLike() {
        $.ajax({
            url:"/article/cancelLike",
            data:{like_user_id:uid,like_article_id:aid,article_like_count:likeCount},
            type:"post",
            dataType:"json",
            success:function (data) {
                if(data.code === 1){
                    location.reload();
                }else{error();}
            },
            error: function (xhr, textStatus, errorThrown) {
                window.location.href = "error";
            }
        });
    }

    /** 收藏 **/
    function collete(title,html) {
        layui.use(['element','jquery','layer'], function(){
            var layer = layui.layer;
            var $ = layui.jquery;
            var element = layui.element;
            layer.open({
                type: 2,
                title: title,
                fix: false, //不固定
                maxmin: true,
                shadeClose: true,
                shade:0.4,
                trigger: 'click',
                area: ['30%', '400px'],
                content: html
            });
        });
        // alert("333");
    }



    /** 取消收藏 **/
    function cancelCollect() {
        $.ajax({
            url:"/user/CancelCollect",
            data:{collection_user_id:uid,collection_article_id:aid},
            type:"post",
            dataType:"json",
            success:function (data) {
                if(data.code === 1){
                    location.reload();
                }else{error();}
            },
            error: function (xhr, textStatus, errorThrown) {
                window.location.href = "error";
            }
        });
    }
</script>
</body>
</html>
