<%--
  Created by IntelliJ IDEA.
  User: dudu
  Date: 2021/4/8
  Time: 17:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html;iso-8859-1" import="java.util.*"
         pageEncoding="utf-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>添加收藏</title>
    <link rel="stylesheet" href="../../layui/css/layui.css">
    <script src="../js/jquery-2.1.1.min.js"></script>
    <script src="../../layui/layui.js"></script>
</head>


<body>
<div style="padding-top: 20px;padding-right: 20px;">

    <form class="layui-form layer-ext-myskin" action="">

        <div class="layui-form-item">
            <label class="layui-form-label">选择收藏夹</label>
            <div class="layui-input-block">
                <select name="city" lay-verify="required" id="select">
                    <option value="">-请选择收藏夹-</option>
                    <c:forEach  items="${CJia}"  var="userItem"  varStatus="userStatus">
                        <option value="${userItem.CJia_id}">${userItem.CJia_name}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <a class="layui-btn" onclick="addCollecte()">立即提交</a>
                <button type="cancel" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>


</div>


<script>
    //Demo

    layui.use('form', function(){
        var form = layui.form;
        //监听提交
        form.on('submit(formDemo)', function(data){
            return false;
        });
    });

    /** 错误提示 */
    function error() {
        layui.use('layer', function() {
            var layer = layui.layer;
            layer.msg('请求失败');
        });
    }

    /** 添加收藏到 收藏夹 **/
    function addCollecte() {
        var cid = document.getElementById("select").value;
        var aid = "${aid}";
        var id = "${session.id}";
        $.ajax({
            url:"/user/addCollection",
            data:{collection_user_id:id,collection_article_id:aid,collection_jia:cid},
            type:"post",
            dataType:"json",
            success:function (data) {
                if(data.code === 1){
                    // 添加收藏夹之后 关闭 弹出窗口
                    var index = parent.layer.getFrameIndex(window.name);
                    //关闭窗口之前刷新 父页面，一定要在 关闭子页面之前刷新父页面
                    parent.location.reload();
                    //关闭窗口
                    parent.layer.close(index);
                }else{
                    error();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                window.location.href = "error";
            }
        });
    }
</script>

</body>
</html>
