<%@ page contentType="text/html;charset=UTF-8" language="java"  isELIgnored="false" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="UTF-8" />	
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />		
	<meta http-equiv="Cache-Control" content="no-transform" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>404</title>
</head>
<style>
.not-found-page .content {
    width: 992px;
    margin: 0 auto 80px;
    display: block;
    position: relative;
}
.not-found-page .content .top-img {
    display: block;
    width: 288px;
    margin: 0 auto;
}
.not-found-page .content .info {
    font-size: 14px;
    margin: 0 auto;
    text-align: center;
}
.not-found-page .content .info h2 {
    font-size: 26px;
    color: #444;
    line-height: 30px;
}
.not-found-page .content .info .state {
    color: #777;
    margin: 7px 0 35px;
    font-size: 16px;
    line-height: 20px;
}
.red-link {
    color: #9A0000;
}
</style>
<body>
<div class="not-found-page">
	<div class="content">
		<img src="../images/img_404.png" class="top-img">
		<div class="info"><h2>很抱歉，你访问的页面出现了错误</h2>
			<p class="state">${errorMsg}</p>
			<div class="search"></div>
		</div>
	</div>
</div>
</body>
</html>