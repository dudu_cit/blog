<%@ page language="java" contentType="text/html;iso-8859-1" import="java.util.*" 
    pageEncoding="utf-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>CIT博客</title>
    <meta name="keywords" content="CIT博客" />
    <meta name="description" content="CIT博客" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/base.css" rel="stylesheet">
    <link href="../css/index.css" rel="stylesheet">
    <link href="../css/model.css" rel="stylesheet">
    <link href="../css/m.css" rel="stylesheet">
    <script src="../js/jquery-2.1.1.min.js"></script>
    <![endif]-->
    <script>

        //判断session 是否存在

        var session = "${session.id}";

        // alert(session);
        $(function () {
            if(session.length ===0 ){
                window.location.href = "login";
                document.getElementById("login_after").style.display="none";
                document.getElementById("login_after2").style.display="none";
                //alert("未登录用户");
            }else{
                document.getElementById("login_before").style.display="none";
                document.getElementById("login_after_name").innerHTML = "${session.username}";
                //alert("已经登陆"+"${session.username}");
            }
        });

        window.onload = function ()
        {
            var oH2 = document.getElementsByTagName("h2")[0];
            var oUl = document.getElementsByTagName("ul")[0];
            oH2.onclick = function ()
            {
                var style = oUl.style;
                style.display = style.display == "block" ? "none" : "block";
                oH2.className = style.display == "block" ? "open" : ""
            }
        }
    </script>
    <style>
        .myblog-name{
            margin: 10px auto 0;
        }
        .myblog-head{
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .myblog-head-line{
            margin-left: 10px;
            margin-right: 10px;
        }
        .collection-line{
            border:1px solid #a1a1a1;
            background-color: #000;
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .myblog-head-div{
            font-size: 24px;
            font-weight: bold;
        }
        .myblog-body{
            margin-top: 20px;
        }
        .myblog-body-content{
            width: 70%;

            padding: 20px 10px 20px 10px;
            margin-top: 10px;
            border:1px solid #dddddd;
            border-radius:15px;
            box-shadow: 1px 1px 3px #ececec;
        }
        .myblog-body-content:hover{
            background-color: #fcfcfc;

        }
        .search{
            padding: 20px 10px 100px;
            border:1px solid #dddddd;
            border-radius:15px;
            box-shadow: 1px 1px 3px #ececec;
        }

        .cloud{
            width: 200px;
        }
        .bloginfo-test{
            color: #888;
            white-space:nowrap;
            /*width:50em;*/
            height: 30px;
            overflow:hidden;
            text-overflow:ellipsis;
        }
    </style>
</head>
<body>
<header>
  <div class="tophead">
    <div class="logo"><a href="/">CIT 博客</a></div>
    <nav class="topnav" id="topnav">
      <ul>
          <li><a href="/admin/index">首页</a></li>
          <li><a href="/admin/blog">博客</a></li>
          <li><a href="/user/collection">收藏</a></li>
          <li><a href="/user/info">动态</a></li>
          <li><a href="/user/messages">消息</a></li>
          <li><a href="/user/markdown">创作</a></li>
          <li><a href="/my-blog">我的</a></li>
          <li id="login_before"><a href="/user/login">登陆/注册</a></li>
          <li id="login_after"><a href="/user/showUserBaseInfo" id="login_after_name">DUDU</a></li>
          <li id="login_after2"><a href="#modal-one" class="out_btn-big">退出登陆</a></li>
      </ul>
    </nav>
  </div>
</header>

<!-- 模态框（Modal） -->
<div class="modal" id="modal-one" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-header">
            <h2>CIT博客提醒您</h2>
            <a href="#" class="out_btn-close" aria-hidden="true">×</a>
        </div>
        <div class="modal-body">
            <p>是否确认退出？一旦退出就需要重新登陆</p>
        </div>
        <div class="modal-footer">
            <a href="/user/outLogin" class="out_btn">确 定</a>
        </div>
    </div>
</div>


<article>
    <h1 class="t_nav"><span>像“草根”一样，紧贴着地面，低调的存在，冬去春来，枯荣无恙。</span><a href="/" class="n1">首页</a><a href="/" class="n2">私信</a></h1>
    <div class="ab_box">
        <div class="avatar"> <img src="../images/avatar.jpg"> </div>
        <div class="myblog-head" style="color: #000;font-weight: bold;font-size: 24px"> ${authorInfoModel.author_name}</div>
        <div class="myblog-head myblog-name">
            <div style="color: #7b7575;font-size: 16px">访问量 : <a style="color: #000;font-weight: bold">${authorInfoModel.access_number}</a></div>
            <div class="myblog-head-line">|</div>
            <div style="color: #7b7575;font-size: 16px">博客数量 : <a style="color: #000;font-weight: bold">${authorInfoModel.blog_number}</a></div>
            <div class="myblog-head-line">|</div>
            <div style="color: #7b7575;font-size: 16px">作者排名 : <a style="color: #000;font-weight: bold">${authorInfoModel.author_order}</a></div>
            <div class="myblog-head-line">|</div>
            <div style="color: #7b7575;font-size: 16px">粉丝数量 : <a style="color: #000;font-weight: bold">${authorInfoModel.fax_number}</a></div>
        </div>
        <div class="myblog-head-div">我的博客</div>
        <div class="collection-line"></div>
        <div class="myblog-body">
            <div class="myblog-body-search">
                <div class="search" style="float: right">
                    <form action="/e/search/index.php" method="post" name="searchform" id="searchform">
                        <input name="keyboard" id="keyboard" class="input_text" value="请输入关键字" style="color: rgb(153, 153, 153);" onfocus="if(value=='请输入关键字'){this.style.color='#000';value=''}" onblur="if(value==''){this.style.color='#999';value='请输入关键字'}" type="text">
                        <input name="show" value="title" type="hidden">
                        <input name="tempid" value="1" type="hidden">
                        <input name="tbname" value="news" type="hidden">
                        <input name="Submit" class="input_submit" value="搜索" type="submit">
                    </form>
                    <div class="cloud" style="float: right">
                        <h2 class="hometitle">标签云</h2>
                        <ul>
                            <!---  标签云 -->
                            <c:forEach  items="${tagModel}"  var="userItem"  varStatus="userStatus">
                                <a href="/">${userItem.tag_name}</a>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
            </div>



            <c:forEach  items="${articleModel_list}"  var="userItem"    end="15" varStatus="userStatus">
                <div class="myblog-body-content">
                    <li> <span class="blogpic"><a href="/"><img src="../images/text02.jpg"></a></span>
                        <h3 class="blogtitle " style="font-size: 18px;font-weight: bold"><a href="/article/blog_content?aid=${userItem.article_id}" class="a-style" target="_blank">${userItem.article_title}</a></h3>
                        <div class=" bloginfo-test">
                            ${userItem.article_summary}
                        </div>
                        <div class="autor">
                        <span class="lm">
                            <a href="/" title="CSS3|Html5" target="_blank" class="classname">${userItem.article_type_name}</a> |
                            <a href="/" title="CSS3|Html5" target="_blank" class="classname">${userItem.article_status_name}</a>
                        </span>
                            <span class="dtime">${userItem.article_create_time}</span>
                            <span class="viewnum">浏览（<a href="/">${userItem.article_view_count}</a>）</span>
                            <span class="dianzan">点赞（<a href="/">${userItem.article_order}</a>）</span>
                            <span class="readmore"><a href="/deleteArticle?article_id=${userItem.article_id}">删除</a></span>
                            <span class="readmore"><a href="/article/blog_content?aid=${userItem.article_id}" target="_blank">阅读原文</a></span>
                            <span class="readmore"><a href="/">编辑</a></span>
                        </div>
                    </li>
                </div>
            </c:forEach>

        </div>

        <div style="margin-top: 500px"></div>
    </div>

</article>




<footer>
    <p>@DUDU <a href="/">CIT博客</a> <a href="/">蜀ICP备11002373号-1</a></p>
</footer>

<script src="../js/nav.js"></script>

</body>
</html>
