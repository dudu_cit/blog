<%@ page language="java" contentType="text/html;iso-8859-1" import="java.util.*" 
    pageEncoding="utf-8" isELIgnored="false" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>CIT博客</title>
    <meta name="keywords" content="CIT博客" />
    <meta name="description" content="CIT博客" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/base.css" rel="stylesheet">
    <link href="../css/index.css" rel="stylesheet">
    <link href="../css/m.css" rel="stylesheet">
    <link href="../css/model.css" rel="stylesheet">
    <script src="../js/jquery-2.1.1.min.js"></script>
    <script src="js/modernizr.js"></script>
    <![endif]-->
    <script>

        //判断session 是否存在

        var session = "${session.id}";

        // alert(session);
        $(function () {
            if(session.length ===0 ){
                window.location.href = "login";
                document.getElementById("login_after").style.display="none";
                document.getElementById("login_after2").style.display="none";
                //alert("未登录用户");
            }else{
                document.getElementById("login_before").style.display="none";
                document.getElementById("login_after_name").innerHTML = "${session.username}";
                //alert("已经登陆"+"${session.username}");
            }
        });

        window.onload = function ()
        {
            var oH2 = document.getElementsByTagName("h2")[0];
            var oUl = document.getElementsByTagName("ul")[0];
            oH2.onclick = function ()
            {
                var style = oUl.style;
                style.display = style.display == "block" ? "none" : "block";
                oH2.className = style.display == "block" ? "open" : ""
            }
        }
    </script>
    <style>

        .collection-body{

        }
        .select-picker-options-list{
            display: inline;
            margin-left: 10px;
            font-family: "Rockwell Extra Bold";
            font-size: 18px;
            color: #000606;
        }
        .select-picker-options{
            padding: 30px 20px 30px 20px;
            margin-top: 10px;
            border:1px solid #dddddd;
            border-radius:15px;
            box-shadow: 1px 1px 3px #ececec;

        }
        .select-picker-options:hover{
            background-color: #eff9ef;
        }
        .collection-line{
            border:1px solid #a1a1a1;
            background-color: #000;
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .collection-head-select{
            display: inline;
            font-size: 18px;
        }
        .collection-head-title{
            font-size: 28px;
        }
        .select-picker-options-a{
            border:1px solid #b7b7b7;
            padding: 2px 2px 2px 2px;
            font-size: 16px;
        }
        .select-picker-options-b{
            font-size: 12px;
            color: #a3a3a3;
            margin-left: 680px;
        }
    </style>
</head>
<body>
<header>
  <div class="tophead">
    <div class="logo"><a href="/">CIT 博客</a></div>
    <nav class="topnav" id="topnav">
      <ul>
          <li><a href="/admin/index">首页</a></li>
          <li><a href="/admin/blog">博客</a></li>
          <li><a href="/user/collection">收藏</a></li>
          <li><a href="/user/info">动态</a></li>
          <li><a href="/user/messages">消息</a></li>
          <li><a href="/user/markdown">创作</a></li>
          <li><a href="/my_blog">我的</a></li>
          <li id="login_before"><a href="/user/login">登陆/注册</a></li>
          <li id="login_after"><a href="/user/showUserBaseInfo" id="login_after_name">DUDU</a></li>
          <li id="login_after2"><a href="#modal-one" class="out_btn-big">退出登陆</a></li>
      </ul>
    </nav>
  </div>
</header>


<!-- 模态框（Modal） -->
<div class="modal" id="modal-one" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-header">
            <h2>CIT博客提醒您</h2>
            <a href="#" class="out_btn-close" aria-hidden="true">×</a>
        </div>
        <div class="modal-body">
            <p>是否确认退出？一旦退出就需要重新登陆</p>
        </div>
        <div class="modal-footer">
            <a href="/user/outLogin" class="out_btn">确 定</a>
        </div>
    </div>
</div>


<article>
    <h1 class="t_nav"><span>像“草根”一样，紧贴着地面，低调的存在，冬去春来，枯荣无恙。</span><a href="/" class="n1">首页</a><a href="/" class="n2">私信</a></h1>
    <div class="ab_box">
        <div class="collection-head">
            <div class="collection-head-select collection-head-title">我的消息</div>
            <div class="collection-head-select" style="margin-left: 800px"><a href="#" class="a-style">删除全部</a></div>
            <div class="collection-head-select" style="margin-left: 20px">|</div>
            <div class="collection-head-select" style="margin-left: 20px"><a href="#" class="a-style">退出</a></div>
        </div>
        <div class="collection-line"></div>
        <div class="collection-body">
            <div class="select-picker-options">

                <div class="select-picker-options-list">
                    <a class="select-picker-options-a">博客</a>
                    <a href="#" style="color:#1e34d2;">jiangming</a>
                    <a href="#" class="a-style">私信给你</a>
                    <a class="select-picker-options-b">2020-3-3 15:30 <a href="#" class="a-style">&nbsp;   &nbsp;    删除</a></a>
                </div>
            </div>
            <div class="select-picker-options">
                <div class="select-picker-options-list">
                    <a class="select-picker-options-a">博客</a>
                    <a href="#" style="color:#1e34d2;">jiangming</a>
                    <a href="#" class="a-style">私信给你</a>
                    <a class="select-picker-options-b">2020-3-3 15:30 <a href="#" class="a-style">&nbsp;   &nbsp;    删除</a></a>
                </div>
            </div>
            <div class="select-picker-options">

                <div class="select-picker-options-list">
                    <a class="select-picker-options-a">博客</a>
                    <a href="#" style="color:#1e34d2;">jiangming</a>
                    <a href="#" class="a-style">私信给你</a>
                    <a class="select-picker-options-b">2020-3-3 15:30 <a href="#" class="a-style">&nbsp;   &nbsp;    删除</a></a>
                </div>
            </div>

        </div>
        <div style="margin-top: 500px"></div>

    </div>
</article>

<footer>
    <p>Design by <a href="/">布拉猫设计</a> <a href="/">蜀ICP备11002373号-1</a></p>
</footer>

<script src="../js/nav.js"></script>
<script>
    function checkAll(){
        //1.获取编号前面的复选框
        var checkAllEle = document.getElementById("checkAll");
        //2.对编号前面复选框的状态进行判断
        if(checkAllEle.checked==true){
            //3.获取下面所有的复选框
            var checkOnes = document.getElementsByName("checkOne");
            //4.对获取的所有复选框进行遍历
            for(var i=0;i<checkOnes.length;i++){
                //5.拿到每一个复选框，并将其状态置为选中
                checkOnes[i].checked=true;
            }
        }else{
            //6.获取下面所有的复选框
            var checkOnes = document.getElementsByName("checkOne");
            //7.对获取的所有复选框进行遍历
            for(var i=0;i<checkOnes.length;i++){
                //8.拿到每一个复选框，并将其状态置为未选中
                checkOnes[i].checked=false;
            }
        }
    }
</script>
</body>
</html>
