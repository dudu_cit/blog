<%@ page language="java" contentType="text/html;iso-8859-1" import="java.util.*" 
    pageEncoding="utf-8" isELIgnored="false" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>CIT博客</title>
    <meta name="keywords" content="CIT博客" />
    <meta name="description" content="CIT博客" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/base.css" rel="stylesheet">
    <link href="../css/index.css" rel="stylesheet">
    <link href="../css/m.css" rel="stylesheet">
    <script src="../js/jquery-2.1.1.min.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
    <script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <![endif]-->
    <script>

        //刷新页面一次,更改 url
        $(document).ready(function () {
            var href = "http://localhost:8011/user/userSet";
            if(location.href !== href){
                location.href=href;
            }
        });

        //判断session 是否存在
        var session = "${session.id}";
         //alert(session);
        $(function () {
            if(session.length ===0 ){
                window.location.href = "login";
                document.getElementById("login_after").style.display="none";
                document.getElementById("login_after2").style.display="none";
                //alert("未登录用户");
            }else{
                document.getElementById("login_before").style.display="none";
                document.getElementById("login_after_name").innerHTML = "${session.username}";
                //alert("已经登陆"+"${session.username}");
            }
        });

        //初始化数据
        $(function () {
            var password = "${session.password}";
            var phone = "${session.phone}";
            var email = "${session.email}";
            if(password.length ===0 ){document.getElementById("password").innerHTML = password;}

            if(phone.length ===0 ){document.getElementById("phone").innerHTML = "未绑定";}
            else { document.getElementById("phone").innerHTML = phone; }

            if(phone.length ===0 ){document.getElementById("email").innerHTML = "未绑定";}
            else { document.getElementById("email").innerHTML = email; }

        });


        window.onload = function ()
        {
            var oH2 = document.getElementsByTagName("h2")[0];
            var oUl = document.getElementsByTagName("ul")[0];
            oH2.onclick = function ()
            {
                var style = oUl.style;
                style.display = style.display == "block" ? "none" : "block";
                oH2.className = style.display == "block" ? "open" : ""
            }
        }
    </script>
    <style>

        .myblog-head{
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .collection-line{
            border:1px solid #a1a1a1;
            background-color: #000;
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .userpage-info{
            margin-left: 20px;
            margin-top: 10px;
            padding: 10px 10px 10px 10px;
            font-size: 18px;
        }
        .userpage-info-a1{
            color: #5c5c5c;
        }
        .userpage-info-a2{
            font-weight: bold;
        }
        .userpage-info-main{
            border:1px solid #dddddd;
            border-radius:15px;
            box-shadow: 1px 1px 3px #ececec;
            padding-top: 15px;
            padding-left: 15px;
            margin-top: 10px;
            padding-bottom: 30px;
        }
        .userpage-info-main:hover{
            background-color: #fcfcfc;

        }
    </style>
</head>
<body>
<header>
  <div class="tophead">
    <div class="logo"><a href="/">CIT 博客</a></div>
    <nav class="topnav" id="topnav">
      <ul>
          <li><a href="/admin/index">首页</a></li>
          <li><a href="/admin/blog">博客</a></li>
          <li><a href="/user/collection">收藏</a></li>
          <li><a href="/user/info">动态</a></li>
          <li><a href="/user/messages">消息</a></li>
          <li><a href="/user/markdown">创作</a></li>
          <li><a href="/my_blog">我的</a></li>
          <li id="login_before"><a href="/user/login">登陆/注册</a></li>
          <li id="login_after"><a href="/user/showUserBaseInfo" id="login_after_name">DUDU</a></li>
          <li id="login_after2"><a href="#" data-toggle="modal"  data-target="#addUserModal3" class="out_btn-big">退出登陆</a></li>
      </ul>
    </nav>
  </div>
</header>



<article>
    <h1 class="t_nav"><span>像“草根”一样，紧贴着地面，低调的存在，冬去春来，枯荣无恙。</span><a href="/user/showUserBaseInfo" class="n2">用户中心</a><a href="/user/userSet" class="n1">账户管理</a></h1>
    <div class="ab_box">
        <div class="avatar"> <img src="../images/avatar.jpg" style="float:left;"> </div>
        <div class="myblog-head" style="color: #000;font-weight: bold;font-size: 24px"> DUDU</div>
        <div class="collection-line"></div>
        <div class="userpage-info-main">
            <h2>账号设置<a class="a-style" style="float: right;margin-right: 15px;font-size: 18px" data-toggle="modal"  data-target="#addUserModal2">编辑</a></h2>
            <div class="userpage-info">
                <a class="userpage-info-a1">密码&nbsp;   &nbsp;    </a>
                <a class="userpage-info-a2" id="password">******</a>
<%--                <a class="a-style" style="float: right;margin-right: 15px">修改密码</a>--%>
            </div>
            <div class="userpage-info">
                <a class="userpage-info-a1">手机&nbsp;   &nbsp;    </a>
                <a class="userpage-info-a2" id="phone">152****2001</a>
<%--                <a class="a-style" style="float: right;margin-right: 15px">修改手机</a>--%>
            </div>
            <div class="userpage-info">
                <a class="userpage-info-a1">邮箱&nbsp;   &nbsp;    </a>
                <a class="userpage-info-a2" id="email">未绑定</a>
<%--                <a class="a-style" style="float: right;margin-right: 15px">绑定邮箱</a>--%>
            </div>
        </div>
        <div style="margin-top: 500px"></div>
    </div>
</article>


<form method="post" action="" class="form-horizontal" role="form" id="form_data3" onsubmit="return check_form()" style="margin: 20px;">
    <div class="modal fade" id="addUserModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×
                    </button>
                    <h4 class="modal-title" id="myModalLabel3">
                        CIT 博客提醒您
                    </h4>
                </div>
                <div class="modal-body">
                    <label  class="col-sm-3 control-label"><p>是否确认退出？一旦退出就需要重新登陆</p></label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                    </button>
                    <a href="/user/outLogin" class="btn btn-primary">确认</a>
                    ><span id="tip3"> </span>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>
</form>

!-- 模态框示例（Modal） -->
<form method="post" action="" class="form-horizontal" role="form" id="form_data2" onsubmit="return setPage_check_form()" style="margin: 20px;">
    <div class="modal fade" id="addUserModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">
                        用户信息
                    </h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">密码</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" id="password1" name="user_passsword2" value="${session.password}"
                                       placeholder="请输入新密码">
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">确认密码</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" id="password2" name="user_passsword2" value="${session.password}"
                                       placeholder="请再次输入新密码" onchange="pwdChange()">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">手机号码</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="user_phone" name="user_phone" value="${session.phone}"
                                       placeholder="请输入手机号码" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-sm-3 control-label">邮箱</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="user_email" value="${session.email}" id="user_email"
                                       placeholder="请输入邮箱" onchange="emChange()">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                    </button>
                    <button type="submit" class="btn btn-primary">
                        立即修改
                    </button><span id="tip1"> </span>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>
</form>
<footer>
    <p>Design by <a href="/">布拉猫设计</a> <a href="/">蜀ICP备11002373号-1</a></p>
</footer>
<script>
    // 教育信息 提交表单
    function setPage_check_form()
    {
        var id = ""+"${session.id}";
        var password1 = document.getElementById("password1").value;
        var phone = document.getElementById("user_phone").value;
        var email = document.getElementById("user_email").value;

        alert("uid:"+id+" ,"+password1+" ,"+phone+" ,"+email+"");

        // 异步提交数据到action/add_action.php页面
        $.ajax(
            {
                url:"/user/updateUser",
                contentType:"application/json;charset=utf-8",
                data:JSON.stringify({id:id,password:password1,phone:phone,email:email}),
                type:"post",
                dataType:"json",
                beforeSend:function()
                {
                    $("#tip").html("<span style='color:blue'>正在处理...</span>");
                    return true;
                },
                success:function(data)
                {
                    if(data.code ==0){
                        alert("修改失败");
                        window.location.href = "/user/reloadSession";
                    }else{
                        alert("修改成功");
                        window.location.href = "/user/reloadSession";
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    /*错误信息处理*/
                    alert("进入error---");
                    alert("状态码："+xhr.status);
                    alert("状态:"+xhr.readyState);//当前状态,0-未初始化，1-正在载入，2-已经载入，3-数据进行交互，4-完成。
                    alert("错误信息:"+xhr.statusText );
                    alert("返回响应信息："+xhr.responseText );//这里是详细的信息
                    alert("请求状态："+textStatus);
                    alert(errorThrown);
                    alert("请求失败");
                    window.location.href = "error";
                },
                complete:function()
                {
                    $('#acting_tips').hide();
                }
            });
        return false;
    }
</script>
<script src="../js/nav.js"></script>
<script>
</script>
</body>
</html>
