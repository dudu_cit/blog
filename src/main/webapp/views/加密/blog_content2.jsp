<%@ page language="java" contentType="text/html;iso-8859-1" import="java.util.*"
         pageEncoding="utf-8" isELIgnored="false" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>CIT博客</title>
    <meta name="keywords" content="CIT博客" />
    <meta name="description" content="CIT博客" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/base.css" rel="stylesheet">
    <link href="../css/index.css" rel="stylesheet">
    <link href="../css/m.css" rel="stylesheet">
    <link href="../css/model.css" rel="stylesheet">
    <script src="../js/jquery-2.1.1.min.js"></script>
    <!--    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">-->
    <!--    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>-->
    <!--    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
    <!--[if lt IE 9]>
    <script src="js/modernizr.js"></script>
    <![endif]-->
    <script>

        //判断session 是否存在

        var session = "${session.id}";

        // alert(session);
        $(function () {
            if(session.length ===0 ){
                window.location.href = "/user/login";
                document.getElementById("login_after").style.display="none";
                document.getElementById("login_after2").style.display="none";
                //alert("未登录用户");
            }else{
                document.getElementById("login_before").style.display="none";
                document.getElementById("login_after_name").innerHTML = "${session.username}";
                //alert("已经登陆"+"${session.username}");
            }
        });

        window.onload = function ()
        {
            var oH2 = document.getElementsByTagName("h2")[0];
            var oUl = document.getElementsByTagName("ul")[0];
            oH2.onclick = function ()
            {
                var style = oUl.style;
                style.display = style.display == "block" ? "none" : "block";
                oH2.className = style.display == "block" ? "open" : ""
            }
        }
    </script>
    <style>
        .news_infos-head{
            background-color: #faf5f8;
            padding-left: 10px;
            padding-top: 10px;
            border-radius: 20px;
        }

        .bs-example {
            background-color: white;
            padding: 10px 25px;
            color: black;
            border: 2px solid #4CAF50;
        }

        .news_infos-content{
            margin-top: 10px;
        }

        .love{
            width: 700px;
            height: 500px;
            margin-left: 30px;

            border:1px solid #dddddd;
            border-radius:15px;
            box-shadow: 1px 1px 3px #ececec;
        }
        .love-title{
            margin-left: 20px;
            font-weight: bold;
            font-family: "Inter", sans-serif;
            font-size: 24px;
            margin-top: 50px;
            font-style: italic;
        }
        .love-content{
            padding-top: 50px;
            margin-left: 100px;
        }
        .love-content p{
            color: #5d5c58;
            font-weight: bold;
            font-family: 'DejaVu Sans Mono', monospace;
            font-size: 20px;

        }
        .love-author{
            float: right;
            margin-right: 30px;
            margin-top: 20px;
            font-weight: bold;
            font-family: "Inter", sans-serif;
            font-size: 24px;
            font-style: italic;
        }
    </style>
</head>
<body>
<header>
    <div class="tophead">
        <div class="logo"><a href="/">CIT 博客</a></div>
        <nav class="topnav" id="topnav">
            <ul>
                <li><a href="/user/index">首页</a></li>
                <li><a href="/user/blog">博客</a></li>
                <li><a href="/user/collection">收藏</a></li>
                <li><a href="/user/info">动态</a></li>
                <li><a href="/user/messages">消息</a></li>
                <li><a href="/user/markdown">创作</a></li>
                <li><a href="/user/my-blog">我的</a></li>
                <li id="login_before"><a href="/user/login">登陆/注册</a></li>
                <li id="login_after"><a href="/user/showUserBaseInfo" id="login_after_name">DUDU</a></li>
                <li id="login_after2"><a href="#modal-one" class="out_btn-big">退出登陆</a></li>
            </ul>
        </nav>
    </div>
</header>


<!-- 模态框（Modal） -->
<div class="modal" id="modal-one" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-header">
            <h2>CIT博客提醒您</h2>
            <a href="#" class="out_btn-close" aria-hidden="true">×</a>
        </div>
        <div class="modal-body">
            <p>是否确认退出？一旦退出就需要重新登陆</p>
        </div>
        <div class="modal-footer">
            <a href="/user/outLogin" class="out_btn">确 定</a>
        </div>
    </div>
</div>

<article>
    <h1 class="t_nav"><span>像“草根”一样，紧贴着地面，低调的存在，冬去春来，枯荣无恙。</span><a href="/" class="n1">首页</a><a href="/" class="n2">博客</a></h1>
    <div class="ab_box">
        <div class="leftbox">
            <div class="newsview">
                <div class="news_infos">
                    <div class="news_infos-head">
                        <h2>CIT 的第二个 1314 </h2>
                        <div class="autor">
                            <span class="lm"><a href="/" title="CSS3|Html5" target="_blank" class="classname">糖糖|橙子</a></span>
                            <span class="dtime">2021-3-14 5：20：00</span>
                            <span class="viewnum">浏览（2021314）</span>
                            <span><a href="/">点赞</a></span>
                            <span><a href="#">收藏</a></span>
                            <span>发布作者：<a href="#">橙子</a></span>
                        </div>
                    </div>
                    <div class="news_infos-content">
                        <div class="love">
                            <div>
                                <p class="love-title">A letter to my baby : </p>
                            </div>
                            <div class="love-content">
                                <P>@糖糖：It's a really lucky thing to know you.</P>
                                <P>@糖糖：What's wrong with being partial to you.</P>
                                <P>@糖糖：Take you time l've been there for you.</P>
                                <P>@糖糖：You're the only one you're special.</P>
                                <P>@糖糖：I knew you were coming,so I waited.</P>
                            </div>
                            <div>
                                <P class="love-author">----  爱加糖的橙子</P>
                                <P></P>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="rightbox">
            <div class="aboutme">
                <h2 class="hometitle">关于作者</h2>
                <div class="avatar"> <img src="../images/yy0.png"> </div>
                <div class="ab_con">
                    <a href="#" class="a-style"><p style="font-family: bold">dudu</p></a>
                    <p>博客： 1万+</p>
                    <p>排名： 10</p>
                    <p>点赞： 1万+</p>
                    <p>访问： 10万+</p>
                    <p>粉丝： 10万+</p>
                    <div>
                        <a href="#"><button class="bs-example">私信</button></a>
                        <a href="#"><button class="bs-example">关注</button></a>
                    </div>
                </div>
            </div>
            <div class="weixin">
                <h2 class="hometitle">微信关注</h2>
                <ul>
                    <img src="../images/wx.jpg">
                </ul>
            </div>
        </div>
    </div>
</article>

<footer>
    <p>@DUDU <a href="/">CIT博客</a> <a href="/">蜀ICP备11002373号-1</a></p>
</footer>
<script src="../js/nav.js"></script>
</body>
</html>
