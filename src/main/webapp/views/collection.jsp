<%@ page language="java" contentType="text/html;iso-8859-1" import="java.util.*"
         pageEncoding="utf-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>CIT博客</title>
    <meta name="keywords" content="CIT博客" />
    <meta name="description" content="CIT博客" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="../css/base.css" rel="stylesheet">
    <link href="../css/index.css" rel="stylesheet">
    <link href="../css/m.css" rel="stylesheet">
    <link href="../css/model.css" rel="stylesheet">
    <script src="../js/jquery-2.1.1.min.js"></script>
    <!--  <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">-->
    <!--  <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>-->
    <!--  <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
    <link rel="stylesheet" href="../fonts/iconfont.css">
    <link href="../css/index-xx.css" rel="stylesheet">
    <link rel="stylesheet" href="../layui/css/layui.css">
    <link rel="stylesheet" href="http://at.alicdn.com/t/font_2086392_ph73t3wx7jc.css">
    <!--[if lt IE 9]>

    <script src="js/modernizr.js"></script>
    <![endif]-->
    <script>

        //判断session 是否存在

        var session = "${session.id}";
        // alert(session);
        $(function () {
            if(session.length ===0 ){

                window.location.href = "login";


                document.getElementById("login_after").style.display="none";
                document.getElementById("login_after2").style.display="none";
                //
            }else{
                document.getElementById("login_before").style.display="none";
                document.getElementById("login_after_name").innerHTML = "${session.username}";
                //alert("已经登陆"+"${session.username}");
            }
        });

        window.onload = function ()
        {
            var oH2 = document.getElementsByTagName("h2")[0];
            var oUl = document.getElementsByTagName("ul")[0];
            oH2.onclick = function ()
            {
                var style = oUl.style;
                style.display = style.display == "block" ? "none" : "block";
                oH2.className = style.display == "block" ? "open" : ""
            }
        }
    </script>
    <style>

        .collection-body{

        }
        .select-picker-options-list{
            display: inline;
            margin-left: 10px;
            font-family: "Rockwell Extra Bold";
            font-size: 18px;
            color: #000606;
        }
        .select-picker-options{
            padding: 30px 20px 30px 20px;
            margin-top: 10px;
            border:1px solid #dddddd;
            border-radius:15px;


        }
        .select-picker-options:hover{
            /*background-color: #eff9ef;*/
            box-shadow: 5px 5px 5px #d7d7d7;
            border-radius:15px;
        }
        .collection-line{
            border:1px solid #a1a1a1;
            background-color: #000;
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .collection-head-select{
            display: inline;
            font-size: 18px;
        }
        .collection-head-title{
            font-size: 28px;
        }

        .blogs{
            margin-bottom: 500px;
        }

        .lm-length{
            width: 300px;
        }
        .collection-jai{
            border:1px solid #dddddd;
            height: 1000px;
            border-radius:15px;
        }
        .collection-createJia{
            width: 100%;
            height: 40px;
            border:1px solid #dddddd;
            padding: 6%;
        }
        .collection-createJia:hover{
            background-color: #f4f4f4;
        }
        .select-css{
            font-size: 16px;
        }



    </style>
</head>
<body>
<header>
    <div class="tophead">
        <div class="logo"><a href="/">CIT 博客</a></div>
        <nav class="topnav" id="topnav">
            <ul>
                <li><a href="/admin/index">首页</a></li>
                <li><a href="/admin/blog">博客</a></li>
                <li><a href="/user/collection">收藏</a></li>
                <li><a href="/user/info">动态</a></li>
                <li><a href="/user/messages">消息</a></li>
                <li><a href="/user/markdown">创作</a></li>
                <li><a href="/my_blog">我的</a></li>
                <li id="login_before"><a href="/user/login">登陆/注册</a></li>
                <li id="login_after"><a href="/user/showUserBaseInfo" id="login_after_name">DUDU</a></li>
                <li id="login_after2"><a href="#modal-one" class="out_btn-big">退出登陆</a></li>
            </ul>
        </nav>
    </div>
</header>


<!-- 模态框（Modal） -->
<div class="modal" id="modal-one" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-header">
            <h2>CIT博客提醒您</h2>
            <a href="#" class="out_btn-close" aria-hidden="true">×</a>
        </div>
        <div class="modal-body">
            <p>是否确认退出？一旦退出就需要重新登陆</p>
        </div>
        <div class="modal-footer">
            <a href="/user/outLogin" class="out_btn">确 定</a>
        </div>
    </div>
</div>


<article>
    <h1 class="t_nav"><span>像“草根”一样，紧贴着地面，低调的存在，冬去春来，枯荣无恙。</span><a href="/" class="n1">首页</a><a href="/" class="n2">私信</a></h1>
    <div class="ab_box">
        <div class="collection-head">
            <div class="collection-head-select collection-head-title">我的收藏</div>
            <div class="collection-head-select" style="margin-left: 800px" onclick="deleteJia()"><a href="#" class="a-style">取消收藏</a></div>
            <div class="collection-head-select" style="margin-left: 20px">|</div>
            <div class="collection-head-select" style="margin-left: 20px"><a href="#" class="a-style">退出</a></div>
        </div>
        <div class="collection-line"></div>
        <div class="blogs">
            <div class="myblog-body-content">
                <div class="collection-body">
                    <c:set var="salary" scope="session" value="${collectionModels}"/>
                    <c:if test="${salary[0] != null }">
                        <div id="collection-content">
                            <div class="select-picker-options">
                                <input type="checkbox" id="checkAll" onclick="checkAll()" class="select-css"/>
                                <div class="select-picker-options-list">全选</div>
                            </div>
                            <c:forEach  items="${collectionModels}"  var="userItem"  varStatus="userStatus">
                                <div class="select-picker-options">
                                    <input type="checkbox" name="checkOne">
                                    <div class="select-picker-options-list" >
							<span class="">
								<a class="ZhuanJia-body-tt">博客</a>
								<a href="/article/blog_content?aid=${userItem.collection_article_id}"  target="_blank" class="  lm-length" style="padding-left: 20px;width: 100px">${userItem.article_title}</a>
								<i class="iconfont iconCollection1" style="color: #fa8905;font-size:24px;float: right;margin-right: 10px" onclick="CancelCollect(${userItem.collection_user_id},${userItem.collection_article_id})"></i>
							</span>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </c:if>
                    <c:if test="${salary[0] == null }">
                        <div id="collection-image">
                            <i class="iconfont iconmeiyoushuju" style="color: #fa8905;font-size:120px;margin-left: 40%"></i>
                            <p style="margin-left: 45%">没有数据鸭</p>
                        </div>
                    </c:if>

                </div>
            </div>
        </div>

        <div class="sidebar collection-jai">

            <div class="collection-createJia">
                <div class="createJia" onclick="createJia()">
                    <i class="iconfont iconwenjianjia" style="font-size:20px; color: #00a8c6">&nbsp;<a style="color: #0a001f">创建收藏夹</a></i>
                </div>

            </div>

            <c:forEach  items="${CJia}"  var="userItem"  varStatus="userStatus">
                <c:set var="CJia" scope="session" value="${CJia_id}"/>
                <c:set var="CJia_ids" scope="session" value="${userItem.CJia_id}"/>
                <div class="collection-createJia" onclick="getNewCJia(${userItem.CJia_id})" id="collection-${userItem.CJia_id}" >
                    <div class="createJia">
                        <i class="iconfont icontubiaozhizuomobanyihuifu-" style="font-size:20px;color: #00a8c6">&nbsp;
                            <a style="color: #0a001f">${userItem.CJia_name}</a>
                            <c:if test="${CJia_ids != 1 }">
                                <button style="color: #0a001f;float: right;margin-right: 30px;" class="layui-btn layui-btn-xs" onclick="deleteJia(${userItem.CJia_id})">删除</button>
                            </c:if>
                        </i>
                    </div>
                </div>


            </c:forEach>
        </div>

    </div>
</article>



<footer>
    <p>Design by <a href="/">布拉猫设计</a> <a href="/">蜀ICP备11002373号-1</a></p>
</footer>

<script src="../js/nav.js"></script>

<script src="../layui/layui.js"></script>

<script>


    /** 设置当前显示的收藏夹 背景颜色 **/
    var CJid = "${CJia_id}";
    var id = "collection-"+CJid;
    document.getElementById(id).style.backgroundColor = "#eeeeee";


    /** 选择复选框 **/
    function checkAll(){
        //1.获取编号前面的复选框
        var checkAllEle = document.getElementById("checkAll");
        //2.对编号前面复选框的状态进行判断
        if(checkAllEle.checked==true){
            //3.获取下面所有的复选框
            var checkOnes = document.getElementsByName("checkOne");
            //4.对获取的所有复选框进行遍历
            for(var i=0;i<checkOnes.length;i++){
                //5.拿到每一个复选框，并将其状态置为选中
                checkOnes[i].checked=true;
            }
        }else{
            //6.获取下面所有的复选框
            var checkOnes = document.getElementsByName("checkOne");
            //7.对获取的所有复选框进行遍历
            for(var i=0;i<checkOnes.length;i++){
                //8.拿到每一个复选框，并将其状态置为未选中
                checkOnes[i].checked=false;
            }
        }
    }

    /** 创建文件夹 **/
    function createJia() {
        //默认prompt
        layui.use(['layer', 'form'], function(){
            var layer = layui.layer,form = layui.form;
            //默认prompt
            layer.prompt({
                    formType: 0,
                    value: '',
                    title: '请输入新收藏夹的名称',
                    btn: ['确定','取消']
                },function(val, index){
                    /** 新增 文件夹 **/
                    var CJia_user_id = "${session.id}";
                    $.ajax({
                        url:"/user/addJia",
                        data:{CJia_name:val,CJia_user_id:CJia_user_id},
                        type:"post",
                        dataType:"json",
                        success:function (data) {
                            layer.msg('添加收藏夹成功');
                            layer.close(index);
                            window.location.href = "/user/collection";
                        },
                        error: function (xhr, textStatus, errorThrown) {

                            window.location.href = "error";
                        }
                    });
                }
            );
        });

    }

    /** 删除收藏夹 **/
    function deleteJia(CJia_id) {
        layui.use(['layer', 'form'], function() {
            var layer = layui.layer, form = layui.form;
            layer.msg('您确认要删除收藏夹？一旦删除收藏夹中的内容也将一并删除。', {
                time: 0 //不自动关闭
                ,btn: ['删除', '取消']
                ,yes: function(index){
                    //AJAX请求
                    /** 新增 文件夹 **/
                    var CJia_user_id = "${session.id}";
                    $.ajax({
                        url:"/user/deleteJia",
                        data:{CJia_id:CJia_id,CJia_user_id:CJia_user_id},
                        type:"post",
                        dataType:"json",
                        success:function (data) {
                            layer.msg('删除成功');
                            layer.close(index);
                            window.location.href = "/user/collection";
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            window.location.href = "error";
                        }
                    });
                }
            });
        });
        // 触发子元素的onclick不同时触发父元素的onclick
        event.cancelBubble=true;
    }

    function error() {
        layui.use('layer', function() {
            var layer = layui.layer;
            layer.msg('请求失败');
        });
    }
    /** 取消收藏 **/
    function CancelCollect(collection_user_id,collection_article_id) {
        layui.use(['layer', 'form'], function() {
            var layer = layui.layer, form = layui.form;
            layer.msg('您确定取消收藏？一旦删除收藏将不克恢复。', {
                time: 0 //不自动关闭
                ,btn: ['删除', '取消']
                ,yes: function(index){
                    //AJAX请求
                    $.ajax({
                        url:"/user/CancelCollect",
                        data:{collection_user_id:collection_user_id,collection_article_id:collection_article_id},
                        type:"post",
                        dataType:"json",
                        success:function (data) {
                            if(data.code === 1){
                                layer.msg('删除成功');
                                layer.close(index);
                                // window.location.href = "/user/collection";
                                location.reload();
                            }else{error();}
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            window.location.href = "error";
                        }
                    });
                }
            });
        });
    }

    /** 查看收藏夹中的内容 **/
    function getNewCJia(CJia_id) {
        //切换颜色 collection-1

        layui.use(['layer', 'form'], function() {
            var layer = layui.layer, form = layui.form;
            window.location.href = "/user/collection?CJia_id="+CJia_id;
            layer.msg('查询成功： '+CJia_id);
        });

    }
</script>
</body>
</html>
