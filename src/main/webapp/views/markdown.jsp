<%@ page language="java" contentType="text/html;iso-8859-1" import="java.util.*" 
    pageEncoding="utf-8" isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>markdwon</title>
    <link rel="stylesheet" href="../markdown/css/editormd.css" />
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<%--    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">--%>
<!--    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>-->
<!--    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
<%--    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">--%>
<!--    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>-->
<!--    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
    <link rel="stylesheet" href="../up_input/css/select.css">
    <link href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">

    <script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
    <script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

    <script src="https://cdn.bootcss.com/showdown/1.3.0/showdown.min.js"></script>  <!-- 转化 js -->
    <link rel="stylesheet" href="https://cdn.bootcss.com/github-markdown-css/2.10.0/github-markdown.min.css">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script>

        //判断session 是否存在
        var session = "${session.id}";
        $(function () {
            if(session.length ===0 ){
                window.location.href = "login";
            }
        });

        window.onload = function ()
        {
            var oH2 = document.getElementsByTagName("h2")[0];
            var oUl = document.getElementsByTagName("ul")[0];
            oH2.onclick = function ()
            {
                var style = oUl.style;
                style.display = style.display == "block" ? "none" : "block";
                oH2.className = style.display == "block" ? "open" : ""
            }
        }
    </script>

    <style>
        .markdown-title{
            margin-top: 20px;
            margin-left: 10px;
            margin-bottom: 30px;
            width: 70%;
        }
        .button{
            float: left;
        }
        .md-head{
            float: left;
        }
        .btn{
            margin-top: 20px;
            margin-left: 10px;
            float: left;
        }
        .md-head-btn1{
            margin-left: 200px;

        }
        .fa-th{
            display: inline;
            margin-top: 10px;
            padding-top: 10px;
        }
    </style>
</head>
<body>
<h3 style="margin-left: 10px">CIT 博客 MarkDown编辑器</h3>
<div class="markdown-title md-head">
    <form class="bs-example bs-example-form" role="form">
        <div class="input-group md-head button">
            <span class="input-group-addon">标题</span>
            <input type="text" class="form-control" placeholder="标题 (1/100字)">
        </div>
    </form>
</div>
<div class="md-head-btn1">
    <a href="/admin/index"><input class="btn btn-success" type="submit" value="返回"></a>
</div>
<div class="md-head-btn2">
                <input class="btn btn-success btn-primary " data-toggle="modal"  data-target="#addUserModal" type="submit" value="发布文章"/>
</div>


<!-- 模态框示例（Modal） -->
<form method="post" action="" class="form-horizontal" role="form" id="form_data" onsubmit="return markdown_check_form()" style="margin: 20px;">
    <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        编辑博客信息
                    </h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="article_title" class="col-sm-3 control-label">文章标题</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="article_title" name="user_id" value=""
                                       placeholder="文章标题">
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-sm-3 control-label">文章标签</label>
                            <div class="col-sm-9">
                                <select name="" id="article_tag" class="form-control">
                                    <option value="" disabled selected hidden>请选择文章的类型</option>
                                    <option>家庭生活</option>
                                    <option>校园生活</option>
                                    <option>IT</option>
                                    <option>养生</option>
                                    <option>科技</option>
                                    <option>学习</option>
                                    <option>美食</option>
                                    <option>SSM</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-sm-3 control-label">文章类型</label>
                            <div class="col-sm-9">
                                <select name="" id="article_status" class="form-control">
                                    <option value="" disabled selected hidden>请选择</option>
                                    <option>原创</option>
                                    <option>转载</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-sm-3 control-label">发布形式</label>
                            <div class="col-sm-9">
                                <select name="" id="article_is_comment" class="form-control">
                                    <option value="" disabled selected hidden>请选择</option>
                                    <option>草稿</option>
                                    <option>公开</option>
                                </select>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                    </button>
                    <button type="submit" class="btn btn-primary">
                        立即发布
                    </button><span id="tip"> </span>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>
<

<div id="layout">
    <div id="editormd">
        <textarea style="display:none;" id="markdown-content">
# MarkDown编辑器基本使用说明

**如果这是您第一次使用MarkDown编辑器，建议先阅读这篇文章了解一下Markdown的基本使用方法。**

## 实时预览、全屏显示

![](/image_editor_upload/20190606013233_25630.png)

**编辑器左边是输入框，右边是实际效果预览，可以点击编辑器上方界面小眼睛按钮来打开或关闭实时预览，如果感觉基本编辑界面太小太窄也可以使用全屏。**

## 插入代码

**请不要在编辑框中直接粘贴代码，请使用代码块。**
- #### 代码块
![](/image_editor_upload/20190606022155_81717.png)
![](/image_editor_upload/20190606024121_61585.png)

	**点击图标“代码块（多语言风格）”，即可在其中输入想要插入的代码。**

	*输入与预览图例：*
	![](/image_editor_upload/20190606022918_54401.png)

	**如上图代码块的开头与结尾都有三个间隔号（键盘左上角Esc下方按键），开头三个间隔号后的“c”即为代码语言。下面是一个例子：**

	```c
	#include "stdio.h"
	int main()
	{
		int a,b;
		while(~scanf("%d%d", &a, &b))printf("%d\n",a+b);
		return 0;
	}
	```

- #### 行内代码
![](/image_editor_upload/20190606040412_74577.png)

	**请点击“行内代码”按钮，再输入代码。**

	*输入与预览图例：*
	![](/image_editor_upload/20190606040626_28452.png)

	**如上图，与代码块不同，行内代码开头和结尾只有一个间隔号。下面是一个例子：**

	`#include "stdio.h" `的作用是引入头文件！

## 标题样式

**标题支持6种样式，可点击H1~H6按钮变更标题样式；**

![](/image_editor_upload/20190606013846_99194.png)

**也可以直接输入“#”来编辑，“#”对应H1,“##”对应H2……**

*输入与预览图例：*
![](/image_editor_upload/20190606014119_69661.png)

## 文字样式

![](/image_editor_upload/20190606014703_35680.png)

**可点击按钮变更样式，或直接输入标记语法。**

*输入与预览图例：*
![](/image_editor_upload/20190606021227_16757.png)

**如上图中直接输入标记语法符号也可以达到想要的效果。下面是几个例子：**

　　*强调文本* _强调文本_
**加粗文本** __加粗文本__
　　==标记文本==
~~删除文本~~
> 引用文本

## 插入图片

![](/image_editor_upload/20190606023726_46319.png)

**点击“添加图片”按钮即可插入想要的图片，但上传有大小限制，图片如果过大请修改本地图片属性后再上传。**

## 插入表格

![](/image_editor_upload/20190606024844_48835.png)

**点击“添加表格”，填写一些基本属性，点击确定得到下图结果：**

![](/image_editor_upload/20190606025647_67742.png)

**在左边输入框中填入一些信息，即可完成表格：**

*输入与预览图例：*
![](/image_editor_upload/20190606025824_42776.png)

**你可以在左边输入框中任意增减图例中的竖线符号来调整表格格式。下面是一个例子：**

| 内容板块 | 大纲内容 | 学习目标 | …… |
| ------------ | ------------ |
| 标准语法 | 标准C语言全语法 | 熟练掌握C语言标准语法 |
| 实验与探究 | C语言特性  | 理解C语言底层原理 | 其他内容 |

## 列表

![](/image_editor_upload/20190606032543_63651.png)

**编辑一个列表，点击图标“无须列表”或“有序列表”，即可编辑内容，编辑好后按回车键，再次点击列表按钮开始编辑下一条。**

*输入与预览图例：*
![](/image_editor_upload/20190606033831_52890.png)

**你可以如上图中根据需求灵活使用其他标记语法与Tab键，改变文字样式与格式。下面是一个例子：**

- #### C语言快速入门

	1. C语言第一个简单实例
	1. 实例说明

- **C语言的数据类型**

	- 变量与常量数据
	- 数据类型和关键字
	- 本章总结与作业

## 科学公式 TeX(KaTeX)

**请在编辑器的预编辑内容中查看公式语法示例**

$$E=mc^2$$

行内的公式$$E=mc^2$$行内的公式。

$$x > y$$

$$\(\sqrt{3x-1}+(1+x)^2\)$$

$$\sin(\alpha)^{\theta}=\sum_{i=0}^{n}(x^i + \cos(f))$$

多行公式：

```math
\displaystyle
\left( \sum\_{k=1}^n a\_k b\_k \right)^2
\leq
\left( \sum\_{k=1}^n a\_k^2 \right)
\left( \sum\_{k=1}^n b\_k^2 \right)
```

```katex
\displaystyle
    \frac{1}{
        \Bigl(\sqrt{\phi \sqrt{5}}-\phi\Bigr) e^{
        \frac25 \pi}} = 1+\frac{e^{-2\pi}} {1+\frac{e^{-4\pi}} {
        1+\frac{e^{-6\pi}}
        {1+\frac{e^{-8\pi}}
         {1+\cdots} }
        }
    }
```

```latex
f(x) = \int_{-\infty}^\infty
    \hat f(\xi)\,e^{2 \pi i \xi x}
    \,d\xi
```

## 关于空格

**想要给一段文字添加多个　*空格符*　？**

**　　如果一定要输入多个连续空格符，可以切换输入法至全角输入（默认半角），然后再输入空格；**

------------

### 来试试编辑一篇文章吧，祝您使用愉快！
        </textarea>
    </div>

</div>


<script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="../markdown/editormd.js"></script>
<script type="text/javascript">



    /** markDown 编辑器 设置 **/
    var testEditor;
    $(function() {
        contextEditor = editormd("editormd", {
            width   : "100%",
            // height  : "100",
            syncScrolling : "single",
            path    : "../markdown/lib/",
            saveHTMLToTextarea : true,
            /**上传图片相关配置如下*/
            imageUpload : true,
            imageFormats : ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
            imageUploadURL : "{:url('widget/editormdPictureUpload')}" //后端的上传图片服务地址
        }).fail(function(){

            alert('editormd加载失败...');
        });

        /*
        // or
        testEditor = editormd({
            id      : "editormd",
            width   : "100%",
            height  : 640,
            path    : "lib/editormd/lib/"
        });
        */
    });

    /** 得到 当前时间，转化格式 yyyy-MM-dd hh:mm:ss **/
    function getDate() {
        // 比如需要这样的格式 yyyy-MM-dd hh:mm:ss
        var date = new Date();
        var Y = date.getFullYear() + '-';
        var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
        var D = date.getDate() + ' ';
        var h = date.getHours() + ':';
        var m = date.getMinutes() + ':';
        var s = date.getSeconds();
        var date = Y+M+D+h+m+s+"";
        return date;
    }

    /** 判断 文章的类型，1：原创，0：转载 **/
    function getArticleStatus(status) {
        if(status === "原创") return 1;
        else return 0;
    }

    /** 判断 文章的发布形式，1：公开，0：草稿 **/
    function getArticleIsComment(status) {
        if(status === "公开")return 1;
        else return 0;
    }


    /** 得到 文章的类型 id **/
    function getArticleType(tag) {
        if(tag === "家庭生活" ) return 1;
        if(tag === "校园生活" ) return 2;
        if(tag === "IT" ) return 3;
        if(tag === "养生") return 4;
        if(tag === "科技") return 5;
        if(tag === "学习") return 6;
        if(tag === "美食") return 7;
        if(tag === "SSM") return 8;
    }


    /** 提交表单 **/
    function markdown_check_form()
    {
        var article_user_id = "${session.id}";
        var article_content = ""+document.getElementById("markdown-content").value;
        var article_title = ""+document.getElementById("article_title").value;
        var article_is_comment  = ""+document.getElementById("article_is_comment").value;
        var article_status = ""+document.getElementById("article_status").value;
        var article_create_time=getDate()+"";
        var article_tag = ""+document.getElementById("article_tag").value;

        //类型转化
        article_is_comment = getArticleIsComment(article_is_comment)+"";
        article_status = getArticleStatus(article_status)+"";
        var article_type_id = getArticleType(article_tag)+"";
        //将文章转化成 html格式
        var converter = new showdown.Converter({tables: true}); //{tables: true}，默认不识别表格，需要加上这个
        article_content = converter.makeHtml(article_content);//转换

        // alert("article_user_id : "+article_user_id);
        // alert("article_title : "+article_title);
         alert("article_content : "+article_content);
        // alert("article_type_id : "+article_type_id);
        // alert("article_status : "+article_status);
        // alert("article_is_comment : "+article_is_comment);
        // alert("article_create_time : "+article_create_time);

        //文章内容不可空
        if(article_content.length === 0){return  false;}
        ///文章标题不可空
        if(article_title.length === 0){
            alert("文章标题不可为空");
            return false;
        }else{
            //发送ajax请求到 处理文章上传 controller 处理器
            $.ajax(
                {
                    url:"${pageContext.request.contextPath }/article/uploadArticle",
                    contentType:"application/json;charset=utf-8",
                    data:JSON.stringify({
                        article_user_id:article_user_id,
                        article_title:article_title,
                        article_content:article_content,
                        article_is_comment:article_is_comment,
                        article_status:article_status,
                        article_create_time:article_create_time,
                        article_type_id:article_type_id
                    }),
                    type:"post",
                    dataType:"json",
                    beforeSend:function()
                    {
                        $("#tip").html("<span style='color:blue'>正在处理...</span>");
                        return true;
                    },
                    success:function(data)
                    {
                        if(data.code ==0){
                            alert("发布失败");
                            window.location.href = "/user/markdown";
                        }else{
                            alert("发布成功: "+data.code);
                            window.location.href = "/article/jumpPage?aid="+data.code;
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        /*错误信息处理*/
                        alert("进入error---");
                        alert("状态码："+xhr.status);
                        alert("状态:"+xhr.readyState);//当前状态,0-未初始化，1-正在载入，2-已经载入，3-数据进行交互，4-完成。
                        alert("错误信息:"+xhr.statusText );
                        alert("返回响应信息："+xhr.responseText );//这里是详细的信息
                        alert("请求状态："+textStatus);
                        alert(errorThrown);
                        alert("请求失败");
                        window.location.href = "error";
                    },
                    complete:function()
                    {
                        $('#acting_tips').hide();
                        return true;
                    }
                });
        }

        return false;
    }

    $(function () { $('#addUserModal').on('hide.bs.modal', function () {
        // 关闭时清空edit状态为add
        $("#act").val("add");
        location.reload();
    })
    });
</script>



<%--<!--下拉菜单-->--%>
<%--<script src="../up_input/js/jquery.min.js"></script>--%>
<%--<script src="../up_input/js/select.js"></script>--%>
<%--<script>--%>
<%--    $(function() {--%>
<%--        $("#demo1").mySelect();--%>
<%--        $("#demo2").mySelect();--%>
<%--    });--%>
<%--</script>--%>
</body>
</html>