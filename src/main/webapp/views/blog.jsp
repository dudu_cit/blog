<%@ page language="java" contentType="text/html;iso-8859-1" import="java.util.*" 
    pageEncoding="utf-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>CIT博客</title>
    <meta name="keywords" content="CIT博客" />
    <meta name="description" content="CIT博客" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/base.css" rel="stylesheet">
    <link href="../css/index.css" rel="stylesheet">
    <link href="../css/m.css" rel="stylesheet">
    <link href="../css/index-xx.css" rel="stylesheet">
    <script src="../js/jquery-2.1.1.min.js"></script>
    <script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
    <script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <!--[if lt IE 9]>
    <script src="js/modernizr.js"></script>
    <![endif]-->
    <script>

        //判断session 是否存在

        var session = "${session.id}";

        // alert(session);
        $(function () {
            if(session.length ===0 ){
                document.getElementById("login_after").style.display="none";
                document.getElementById("login_after2").style.display="none";
                //alert("未登录用户");
            }else{
                document.getElementById("login_before").style.display="none";
                document.getElementById("login_after_name").innerHTML = "${session.username}";
                //alert("已经登陆"+"${session.username}");
            }
        });

        window.onload = function ()
        {
            var oH2 = document.getElementsByTagName("h2")[0];
            var oUl = document.getElementsByTagName("ul")[0];
            oH2.onclick = function ()
            {
                var style = oUl.style;
                style.display = style.display == "block" ? "none" : "block";
                oH2.className = style.display == "block" ? "open" : ""
            }
        }
    </script>
    <style>
        .bloginfo-test{
            white-space:nowrap;
            /*width:50em;*/
            height: 30px;
            overflow:hidden;
            text-overflow:ellipsis;
        }
        .bloginfo-test2{
            white-space:nowrap;
            /*width:50em;*/
            height: 60px;
            overflow:hidden;
            text-overflow:ellipsis;
        }
    </style>
</head>
<body>
<header>
  <div class="tophead">
    <div class="logo"><a href="/">CIT 博客</a></div>
    <nav class="topnav" id="topnav">
      <ul>
          <li><a href="/admin/index">首页</a></li>
          <li><a href="/admin/blog">博客</a></li>
          <li><a href="/user/collection">收藏</a></li>
          <li><a href="/user/info">动态</a></li>
          <li><a href="/user/messages">消息</a></li>
          <li><a href="/user/markdown">创作</a></li>
          <li><a href="/my_blog">我的</a></li>
          <li id="login_before"><a href="/user/login">登陆/注册</a></li>
          <li id="login_after"><a href="/user/showUserBaseInfo" id="login_after_name">DUDU</a></li>
          <li id="login_after2"><a href="#-one" data-toggle="modal"  data-target="#addUserModal3" class="out_btn-big">退出登陆</a></li>
      </ul>
    </nav>
  </div>
</header>

<%--退出框--%>
<form method="post" action="" class="form-horizontal" role="form" id="form_data3" onsubmit="return blog_check_form3()" style="margin: 20px;">
    <div class="modal fade" id="addUserModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×
                    </button>
                    <h4 class="modal-title" id="myModalLabel3">
                        CIT 博客提醒您
                    </h4>
                </div>
                <div class="modal-body">
                    <p>是否确认退出？一旦退出就需要重新登陆</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                    </button>
                    <a href="/user/outLogin" class="btn btn-primary">确认</a>
                    <span id="tip3"> </span>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>
</form>

<article>
    <h1 class="t_nav"><span>不要轻易放弃。学习成长的路上，我们长路漫漫，只因学无止境。 </span><a href="/" class="n1">网站首页</a><a href="/" class="n2">学无止境</a></h1>
    <div class="blogs">
        <c:forEach  items="${authorInfoModel1}"  var="userItem"  end="15" varStatus="userStatus">
            <div class="myblog-body-content">
                <li> <span class="blogpic"><a href="/"><img src="../images/zd02.jpg"></a></span>
                    <h3 class="blogtitle " style="font-size: 18px;font-weight: bold"><a href="/article/blog_content?aid=${userItem.article_id}" class="a-style">${userItem.article_title}</a></h3>
                    <div class="bloginfo-test">
                            ${userItem.article_summary}
                    </div>
                    <div class="autor">
                        <span class="lm">
                            <a href="/" title="CSS3|Html5" target="_blank" class="classname">${userItem.article_type_name}</a> |
                            <a href="/" title="CSS3|Html5" target="_blank" class="classname">${userItem.article_status_name}</a>
                        </span>
                        <span class="dtime">${userItem.article_create_time}</span>
                        <span class="viewnum">浏览（<a href="/">${userItem.article_view_count}</a>）</span>
                        <span class="dianzan">点赞（<a href="/">${userItem.article_order}</a>）</span>
                        <span class="readmore"><a href="/article/blog_content?aid=${userItem.article_id}">阅读原文</a></span>
                    </div>
                </li>
            </div>
        </c:forEach>

<%--    底部分页--%>
        <div class="pagelist">
            <a href="/download/index_2.html">首页</a>
            <a title="Total record">上一页</a>&nbsp;&nbsp;&nbsp;
            <a href="/download/index_2.html">1</a>&nbsp;
            <a href="/download/index_2.html">2</a>&nbsp;
            <a href="/download/index_2.html">3</a>&nbsp;
            <span style="font-size: 28px;font-weight: bold">...</span>
            <a href="/download/index_2.html">下一页</a>&nbsp;
            <a href="/download/index_2.html">尾页</a>
        </div>
    </div>


    <div class="sidebar">

        <%--    搜索框--%>
        <div class="search">
            <form action="/e/search/index.php" method="post" name="searchform" id="searchform">
                <input name="keyboard" id="keyboard" class="input_text" value="请输入关键字" style="color: rgb(153, 153, 153);" onfocus="if(value=='请输入关键字'){this.style.color='#000';value=''}" onblur="if(value==''){this.style.color='#999';value='请输入关键字'}" type="text">
                <input name="show" value="title" type="hidden">
                <input name="tempid" value="1" type="hidden">
                <input name="tbname" value="news" type="hidden">
                <input name="Submit" class="input_submit" value="搜索" type="submit">
            </form>
        </div>

        <%--    5. 便签云--%>
        <div class="cloud">
            <h2 class="hometitle">标签云</h2>
            <ul>
                <c:forEach  items="${tagModel}"  var="userItem"  varStatus="userStatus">
                    <a href="/">${userItem.tag_name}</a>
                </c:forEach>
            </ul>
        </div>

<%--            栏目导航--%>
        <div class="lmnav">
            <h2 class="hometitle">栏目导航</h2>
            <ul class="navbor">
                <li><a href="#">关于我</a></li>
                <li><a href="share.html">模板分享</a>
                    <ul>
                        <li><a href="list.html">个人博客模板</a></li>
                        <li><a href="#">HTML5模板</a></li>
                    </ul>
                </li>
                <li><a href="list.html">学无止境</a>
                    <ul>
                        <li><a href="list.html">学习笔记</a></li>
                        <li><a href="#">HTML5+CSS3</a></li>
                        <li><a href="#">网站建设</a></li>
                    </ul>
                </li>
                <li><a href="#">慢生活</a></li>
            </ul>
        </div>


        <%--    热门博客  --%>
        <div class="paihang">
            <h2 class="hometitle">热门博客</h2>
            <ul>
                <c:forEach  items="${authorInfoModel2}"  var="userItem"  end="10" varStatus="userStatus">
                    <li><b><a class="ZhuanJia-body-tt">${userStatus.count}</a><a href="/article/blog_content?aid=${userItem.article_id}" target="_blank">${userItem.article_title}</a></b>
                        <p class="bloginfo-test2"><i><img src="../images/t02.jpg"></i>${userItem.article_summary}</p>
                    </li>
                </c:forEach>
            </ul>
        </div>


        <div class="weixin">
            <h2 class="hometitle">官方微信</h2>
            <ul>
                <img src="../images/wx.jpg">
            </ul>
        </div>
    </div>
</article>
<footer>
    <p>@DUDU <a href="/">CIT博客</a> <a href="/">蜀ICP备11002373号-1**</a></p>
</footer>
<script src="../js/nav.js"></script>
</body>
</html>
