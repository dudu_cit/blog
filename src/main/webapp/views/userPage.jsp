<%@ page language="java" contentType="text/html;iso-8859-1" import="java.util.*" 
    pageEncoding="utf-8" isELIgnored="false" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>CIT博客</title>
    <meta name="keywords" content="CIT博客" />
    <meta name="description" content="CIT博客" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/base.css" rel="stylesheet">
    <link href="../css/index.css" rel="stylesheet">
    <link href="../css/m.css" rel="stylesheet">

    <script src="../js/jquery-2.1.1.min.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
    <script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <![endif]-->
    <script>


        //刷新页面一次,更改 url
        $(document).ready(function () {
            var href = "http://localhost:8011/user/showUserBaseInfo";
            if(location.href !== href){
                location.href=href;
            }
        });

        //判断session 是否存在
        var session = "${session.id}";
        //alert(session);
        $(function () {
            if(session.length ===0 ){
                window.location.href = "login";
                document.getElementById("login_after").style.display="none";
                document.getElementById("login_after2").style.display="none";
                //alert("未登录用户");
            }else{
                document.getElementById("login_before").style.display="none";
                document.getElementById("login_after_name").innerHTML = "${session.username}";
                //alert("已经登陆"+"${session.username}");
            }
        });



        window.onload = function ()
        {
            var oH2 = document.getElementsByTagName("h2")[0];
            var oUl = document.getElementsByTagName("ul")[0];
            oH2.onclick = function ()
            {
                var style = oUl.style;
                style.display = style.display == "block" ? "none" : "block";
                oH2.className = style.display == "block" ? "open" : ""
            }
        }
    </script>
    <style>

        .myblog-head{
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .collection-line{
            border:1px solid #a1a1a1;
            background-color: #000;
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .userpage-info{
            margin-left: 20px;
            margin-top: 20px;
            padding: 15px 15px 15px 15px;
            font-size: 18px;
        }
        .userpage-info-a1{
            color: #5c5c5c;
        }
        .userpage-info-a2{
            font-weight: bold;
        }
        .userpage-info-main{
            border:1px solid #dddddd;
            border-radius:15px;
            box-shadow: 1px 1px 3px #ececec;
            padding-top: 15px;
            padding-left: 15px;
            margin-top: 10px;
        }
        .userpage-info-main:hover{
            background-color: #fcfcfc;

        }
    </style>
</head>
<body>
<header>
  <div class="tophead">
    <div class="logo"><a href="/">CIT 博客</a></div>

    <nav class="topnav" id="topnav">
      <ul>
          <li><a href="/admin/index">首页</a></li>
          <li><a href="/admin/blog">博客</a></li>
          <li><a href="/user/collection">收藏</a></li>
          <li><a href="/user/info">动态</a></li>
          <li><a href="/user/messages">消息</a></li>
          <li><a href="/user/markdown">创作</a></li>
          <li><a href="/my_blog">我的</a></li>
          <li id="login_before"><a href="/user/login">登陆/注册</a></li>
          <li id="login_after"><a href="/user/showUserBaseInfo" id="login_after_name">${session.username}</a></li>
          <li id="login_after2"><a href="#" data-toggle="modal"  data-target="#addUserModal3" class="out_btn-big">退出登陆</a></li>
      </ul>
    </nav>
  </div>

</header>

<%--<!-- 模态框（Modal） -->--%>
<%--<div class="modal" id="modal-one" aria-hidden="true">--%>
<%--    <div class="modal-dialog">--%>
<%--        <div class="modal-header">--%>
<%--            <h2>CIT博客提醒您</h2>--%>
<%--            <a href="#" class="out_btn-close" aria-hidden="true">×</a>--%>
<%--        </div>--%>
<%--        <div class="modal-body">--%>
<%--            <p>是否确认退出？一旦退出就需要重新登陆</p>--%>
<%--        </div>--%>
<%--        <div class="modal-footer">--%>
<%--            <a href="user/outLogin" class="out_btn">确 定</a>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--</div>--%>


<article>
    <h1 class="t_nav"><span>像“草根”一样，紧贴着地面，低调的存在，冬去春来，枯荣无恙。</span><a href="/user/showUserBaseInfo" class="n1">用户中心</a><a href="/user/userSet" class="n2">账户管理</a></h1>
    <div class="ab_box">
        <div class="avatar"> <img src="../images/avatar.jpg" style="float:left;"> </div>
        <div class="myblog-head" style="color: #000;font-weight: bold;font-size: 24px" id="name1"> 昵称</div>
        <div class="collection-line"></div>
        <div class="userpage-info-main">

            <h2>基本信息<a class="a-style" style="float: right;margin-right: 15px;font-size: 18px" data-toggle="modal"  data-target="#addUserModal" id="baseInfoEditBtn">编辑</a></h2>
            <div class="userpage-info"><a class="userpage-info-a1">用户昵称&nbsp;   &nbsp;    </a><a class="userpage-info-a2" id="name">DUDU</a></div>
            <div class="userpage-info"><a class="userpage-info-a1">用户ID&nbsp;   &nbsp;    &nbsp;      &nbsp;    </a><a class="userpage-info-a2" id="uid">DUDU123465</a></div>
            <div class="userpage-info"><a class="userpage-info-a1">用户性别&nbsp;   &nbsp;    </a><a class="userpage-info-a2" id="sex">男</a></div>
            <div class="userpage-info"><a class="userpage-info-a1">用户年龄&nbsp;   &nbsp;    </a><a class="userpage-info-a2" id="age">23</a></div>
            <div class="userpage-info"><a class="userpage-info-a1">个人简介&nbsp;   &nbsp;    </a><a class="userpage-info-a2" id="introduction">SSM</a></div>
            <div class="userpage-info"><a class="userpage-info-a1">所在地址&nbsp;   &nbsp;    </a><a class="userpage-info-a2" id="address">湖南岳阳</a></div>
            <div class="userpage-info"><a class="userpage-info-a1">出生年月&nbsp;   &nbsp;    </a><a class="userpage-info-a2" id="birthday">2020 06</a></div>

        </div>
        <div class="userpage-info-main">
            <h2>教育信息<a class="a-style" style="float: right;margin-right: 15px;font-size: 18px" data-toggle="modal"  data-target="#addUserModal2">编辑</a></h2>
            <div class="userpage-info"><a class="userpage-info-a1">学校/公司&nbsp;   &nbsp;    </a><a class="userpage-info-a2" id="sch">湖南理工大学</a></div>
            <div class="userpage-info"><a class="userpage-info-a1">专业/职业&nbsp;   &nbsp;    </a><a class="userpage-info-a2" id="pro">软件工程</a></div>
            <div class="userpage-info"><a class="userpage-info-a1">学历/经历&nbsp;   &nbsp;    </a><a class="userpage-info-a2" id="edu">本科</a></div>
        </div>
        <div style="margin-top: 500px"></div>
    </div>
</article>

<%--退出框--%>
<form method="post" action="" class="form-horizontal" role="form" id="form_data3" onsubmit="return check_form3()" style="margin: 20px;">
    <div class="modal fade" id="addUserModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×
                    </button>
                    <h4 class="modal-title" id="myModalLabel3">
                        CIT 博客提醒您
                    </h4>
                </div>
                <div class="modal-body">
                    <p>是否确认退出？一旦退出就需要重新登陆</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                    </button>
                    <a href="/user/outLogin" class="btn btn-primary">确认</a>
                    <span id="tip3"> </span>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>
</form>

<!-- 基本信息 编辑框 -->
<form method="post" action="" class="form-horizontal" role="form" id="form_data1" onsubmit="return check_form()" style="margin: 20px;">
    <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        用户信息
                    </h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="user_id" class="col-sm-3 control-label">用户昵称</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="user_name" name="user_name" value="${userInfo_session.name}"
                                       placeholder="请输入用户名">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="user_id" class="col-sm-3 control-label">用户ID</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="user_id" name="user_id" value="${session.username}"
                                       placeholder="请输入用户ID" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-sm-3 control-label">用户性别</label>
                            <div class="col-sm-9">
<%--                                <input type="text" class="form-control" name="user_sex" value="${userInfo_session.sex}" id="user_sex"--%>
<%--                                       placeholder="用户性别">--%>
                                <select name="" id="user_sex" class="form-control">
                                    <option value="${userInfo_session.sex}" disabled selected hidden>请选择性别</option>
                                    <option>男</option>
                                    <option>女</option>
                                </select>
                            </div>
                        </div>



                        <div class="form-group">
                            <label  class="col-sm-3 control-label">用户年龄</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="user_age" value="${userInfo_session.age}" id="user_age"
                                       placeholder="用户年龄">
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">用户简介</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="user_info" value="${userInfo_session.introduction}" id="user_introduction"
                                       placeholder="用户简介">
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">用户地址</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="user_address" value="${userInfo_session.address}" id="user_address"
                                       placeholder="用户地址">
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">用户生日</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="user_birth" value="${userInfo_session.birthday}" id="user_birthday"
                                       placeholder="用户生日">
                            </div>

                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                    </button>
                    <button type="submit" class="btn btn-primary">
                        立即修改
                    </button><span id="tip"> </span>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>
</form>

<!-- 教育信息 编辑框 -->
<form method="post" action="" class="form-horizontal" role="form" id="form_data2" onsubmit="return check_form2()" style="margin: 20px;">
    <div class="modal fade" id="addUserModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">
                        用户信息
                    </h4>
                </div>

                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="user_id" class="col-sm-3 control-label">学校/公司</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="user_school" name="user_school" value="${userEdu_session.sch}"
                                       placeholder="学校/公司">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="user_id" class="col-sm-3 control-label">专业/职业</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="user_work" name="user_work" value="${userEdu_session.pro}"
                                       placeholder="专业/职业">
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-sm-3 control-label">学历/经历</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="user_experience" value="${userEdu_session.edu}" id="user_experience"
                                       placeholder="学历/经历">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                    </button>
                    <button type="submit" class="btn btn-primary">
                        立即修改
                    </button><span id="tip1"> </span>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>
</form>

<footer>
    <p>Design by <a href="/">布拉猫设计</a> <a href="/">蜀ICP备11002373号-1</a></p>
</footer>

<script>


    //页面初始化加载信息
    var name = "${userInfo_session.name}";
    var uid = "${userInfo_session.uid}";
    var sex = "${userInfo_session.sex}";
    var age = "${userInfo_session.age}";
    var introduction = "${userInfo_session.introduction}";
    var address = "${userInfo_session.address}";
    var birthday = "${userInfo_session.birthday}";

    //教育信息
    var sch = "${userEdu_session.sch}";
    var pro = "${userEdu_session.pro}";
    var edu = "${userEdu_session.edu}";

    if(name.length !== 0){document.getElementById("name").innerHTML = ""+name;document.getElementById("name1").innerHTML = ""+name}else{document.getElementById("name").innerHTML = "未设置";}
    if(uid.length !== 0){document.getElementById("uid").innerHTML = ""+"${session.username}";}else{document.getElementById("uid").innerHTML = "未设置";}
    if(sex.length !== 0){document.getElementById("sex").innerHTML = ""+sex;}else{document.getElementById("sex").innerHTML = "未设置";}
    if(age.length !== 0){document.getElementById("age").innerHTML = ""+age;}else{document.getElementById("age").innerHTML = "未设置";}
    if(introduction.length !==0 ){document.getElementById("introduction").innerHTML = ""+introduction;} else {document.getElementById("introduction").innerHTML = "未设置";}
    if(introduction.length !==0 ){document.getElementById("address").innerHTML = ""+address;} else {document.getElementById("address").innerHTML = "未设置";}
    if(sch.length !==0 ){document.getElementById("sch").innerHTML = ""+sch;} else {document.getElementById("sch").innerHTML = "未设置";}
    if(pro.length !==0 ){document.getElementById("pro").innerHTML = ""+pro;} else {document.getElementById("pro").innerHTML = "未设置";}
    if(edu.length !==0 ){document.getElementById("edu").innerHTML = ""+edu;} else {document.getElementById("edu").innerHTML = "未设置";}


    // 基本信息 提交表单
    function check_form()
    {
        var uid = ""+"${userInfo_session.uid}";
        var name = document.getElementById("user_name").value;
        var sex = document.getElementById("user_sex").value;
        var age = document.getElementById("user_age").value;
        var introduction = document.getElementById("user_introduction").value;
        var address = document.getElementById("user_address").value;
        var birthday = document.getElementById("user_birthday").value;

        // alert("name:"+name+" ,"+uid+" ,"+sex+" ,"+age+" ,"+introduction+" ,"+ address+" ,"+birthday);

        if(name.length === 0){
            alert("用户名不能为空");
            return false;
        }else{
            // 异步提交数据到action/add_action.php页面
            $.ajax(
                {
                    url:"/user/updateUserBaseInfo",
                    contentType:"application/json;charset=utf-8",
                    data:JSON.stringify({uid:uid,name:name,sex:sex,age:age,introduction:introduction,address:address,birthday:birthday}),
                    type:"post",
                    dataType:"json",
                    beforeSend:function()
                    {
                        $("#tip").html("<span style='color:blue'>正在处理...</span>");
                        return true;
                    },
                    success:function(data)
                    {
                        if(data.code ==0){
                            alert("修改失败");
                            window.location.href = "userPage";
                        }else{
                            alert("修改成功");
                            window.location.href = "userPage";
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        /*错误信息处理*/
                        alert("进入error---");
                        alert("状态码："+xhr.status);
                        alert("状态:"+xhr.readyState);//当前状态,0-未初始化，1-正在载入，2-已经载入，3-数据进行交互，4-完成。
                        alert("错误信息:"+xhr.statusText );
                        alert("返回响应信息："+xhr.responseText );//这里是详细的信息
                        alert("请求状态："+textStatus);
                        alert(errorThrown);
                        alert("请求失败");
                        window.location.href = "error";
                    },
                    complete:function()
                    {
                        $('#acting_tips').hide();
                    }
                });
        }
        return false;
    }

    // 教育信息 提交表单
    function check_form2()
    {
        var uid = ""+"${userInfo_session.uid}";
        var sch = document.getElementById("user_school").value;
        var pro = document.getElementById("user_work").value;
        var edu = document.getElementById("user_experience").value;

        alert("uid:"+uid+" ,"+sch+" ,"+pro+" ,"+edu+"");

        // 异步提交数据到action/add_action.php页面
        $.ajax(
            {
                url:"/user/updateUserEduInfo",
                contentType:"application/json;charset=utf-8",
                data:JSON.stringify({uid:uid,sch:sch,pro:pro,edu:edu}),
                type:"post",
                dataType:"json",
                beforeSend:function()
                {
                    $("#tip").html("<span style='color:blue'>正在处理...</span>");
                    return true;
                },
                success:function(data)
                {
                    if(data.code ==0){
                        alert("修改失败");
                        window.location.href = "userPage";
                    }else{
                        alert("修改成功");
                        window.location.href = "userPage";
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    /*错误信息处理*/
                    alert("进入error---");
                    alert("状态码："+xhr.status);
                    alert("状态:"+xhr.readyState);//当前状态,0-未初始化，1-正在载入，2-已经载入，3-数据进行交互，4-完成。
                    alert("错误信息:"+xhr.statusText );
                    alert("返回响应信息："+xhr.responseText );//这里是详细的信息
                    alert("请求状态："+textStatus);
                    alert(errorThrown);
                    alert("请求失败");
                    window.location.href = "error";
                },
                complete:function()
                {
                    $('#acting_tips').hide();
                }
            });
        return false;
    }

    $(function () { $('#addUserModal').on('hide.bs.modal', function () {
        // 关闭时清空edit状态为add
        $("#act").val("add");
        location.reload();
    })
    });
</script>
<script src="../js/nav.js"></script>
<script>
</script>
</body>
</html>
