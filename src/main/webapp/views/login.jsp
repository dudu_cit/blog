
<%@ page language="java" contentType="text/html;iso-8859-1" import="java.util.*" 
    pageEncoding="utf-8" isELIgnored="false" %>
<%@ page import="com.cm.model.UserModel" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>CIT博客</title>
    <meta name="keywords" content="CIT博客" />
    <meta name="description" content="CIT博客" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/base.css" rel="stylesheet">
    <link href="../css/index.css" rel="stylesheet">
    <link href="../css/m.css" rel="stylesheet">

    <!--    <link rel="stylesheet" href="login2/css/reset.min.css">-->
    <link rel="stylesheet" href="../login2/css/style.css">
    <script type="text/javascript" src="../js/jquery-2.1.1.min.js"></script>

    <![endif]-->
    <script type="text/javascript">


       // alert("1");
        //判断session 是否存在

        var session = "${session.id}";
        // alert(session);
        $(function () {
            if(session.length ===0 ){
                document.getElementById("login_after").style.display="none";
                //alert("未登录用户");
            }else{
                document.getElementById("login_before").style.display="none";
                document.getElementById("login_after_name").innerHTML = "${session.username}";
                //alert("已经登陆"+"${session.username}");
            }
        });

        window.onload = function ()
        {
            var oH2 = document.getElementsByTagName("h2")[0];
            var oUl = document.getElementsByTagName("ul")[0];
            oH2.onclick = function ()
            {
                var style = oUl.style;
                style.display = style.display == "block" ? "none" : "block";
                oH2.className = style.display == "block" ? "open" : ""
            }
        };
    </script>

</head>
<body>
<header>
    <div class="tophead">
        <div class="logo"><a href="/">CIT 博客</a></div>
        <nav class="topnav" id="topnav">
            <ul>
                <li><a href="/admin/index">首页</a></li>
                <li><a href="/admin/blog">博客</a></li>
                <li><a href="/user/collection">收藏</a></li>
                <li><a href="/user/info">动态</a></li>
                <li><a href="/user/messages">消息</a></li>
                <li><a href="/user/markdown">创作</a></li>
                <li><a href="/user/my-blog">我的</a></li>
                <li id="login_before"><a href="/user/login">登陆/注册</a></li>
                <li id="login_after"><a href="/user/showUserBaseInfo" id="login_after_name">DUDU</a></li>
            </ul>
        </nav>
    </div>
</header>



<article>
    <h1 class="t_nav"></h1>
    <div class="ab_box">
        <h1 class="t_nav"><span>像“草根”一样，紧贴着地面，低调的存在，冬去春来，枯荣无恙。</span><a href="login.jsp" class="n1">登陆</a><a href="registration.jsp" class="n2">注册</a></h1>

        <div class="container">
            <div class="panel">
                <div class="panel-header">
                    <div class="panel-header-logo">
                        <img src="../images/bi04.jpg" width="126" height="38" />
                    </div>
                    <h1 class="panel-header-title">登录 CIT博客</h1>
                    <p class="panel-header-info">没有CIT博客帐户？ <a href="/user/registration" class="link">注册CIT博客</a></p>
                </div>
                <form class="form" action="javascript:void(0);">
                    <div class="input">
                        <label class="input-label">账号</label>
                        <input type="text" class="input-field" name="username" id="username" value="DUDU"/>
                    </div>
                    <div class="input">
                        <label class="input-label">密码</label>
                        <input type="password" class="input-field" name="password" id="password" value="123"/>
                    </div>
                    <div class="input">
                        <button class="input-submit" id="login_btn">登录</button>
                    </div>
                </form>
                <div class="panel-footer">
                    <p class="panel-footer-info">忘记密码？ <a href="#" class="link">找回密码</a></p>
                </div>
            </div>
        </div>

        <div style="margin-top: 100px"></div>
    </div>


</article>

<footer>
    <p>@DUDU <a href="/">CIT博客</a> <a href="/">蜀ICP备11002373号-1</a></p>
</footer>

<script>

    $(function () {
        $("#login_btn").click(function () {


            var username = document.getElementById("username").value;
            var password = document.getElementById('password').value;
            //alert("u:"+username);
            if(username.length === 0 || password.length === 0){
                alert("账号或密码为空");
                //清空输入框
                // document.getElementById("username").innerText = "";
                // document.getElementById("password").innerText = "";
            }else {
                // alert("2");
                $.ajax({
                    url:"userLogin",
                    contentType:"application/json;charset=utf-8",
                    data:JSON.stringify({username:username,password:password}),
                    type:"post",
                    dataType:"json",
                    success:function (data) {
                        if(data.code ==0){
                            alert(data.msg);
                        }else{
                            //alert("登陆成功");
                            var from_url = document.referrer;
                            //from_url.slice(21) 表示去除 路径中的 http://localhost:8011
                            window.location.href = ""+from_url.slice(21);
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        /*错误信息处理*/
                        window.location.href = "error";
                    }
                });
            }
        });
    });



</script>
</body>
</html>
