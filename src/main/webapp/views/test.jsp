<%@ page language="java" contentType="text/html;iso-8859-1" import="java.util.*"
         pageEncoding="utf-8" isELIgnored="false" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>CIT博客</title>
    <meta name="keywords" content="CIT博客" />
    <meta name="description" content="CIT博客" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/base.css" rel="stylesheet">
    <link href="../css/index.css" rel="stylesheet">
    <link href="../css/m.css" rel="stylesheet">

    <script src="../js/jquery-2.1.1.min.js"></script>
    <script src="js/modernizr.js"></script>

    <script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../layui/css/layui.css">
    <script src="../layui/layui.js"></script>
    <![endif]-->
    <script>

    </script>
    <style>

        .myblog-head{
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .collection-line{
            border:1px solid #a1a1a1;
            background-color: #000;
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .userpage-info{
            margin-left: 20px;
            margin-top: 20px;
            padding: 15px 15px 15px 15px;
            font-size: 18px;
        }
        .userpage-info-a1{
            color: #5c5c5c;
        }
        .userpage-info-a2{
            font-weight: bold;
        }
        .userpage-info-main{
            border:1px solid #dddddd;
            border-radius:15px;
            box-shadow: 1px 1px 3px #ececec;
            padding-top: 15px;
            padding-left: 15px;
            margin-top: 10px;
        }
        .userpage-info-main:hover{
            background-color: #fcfcfc;

        }
    </style>
</head>
<body>
<header>
    <div class="tophead">
        <div class="logo"><a href="/">CIT 博客</a></div>

        <nav class="topnav" id="topnav">
            <ul>
                <li><a href="/admin/index">首页</a></li>
                <li><a href="/admin/blog">博客</a></li>
                <li><a href="/user/collection">收藏</a></li>
                <li><a href="/user/info">动态</a></li>
                <li><a href="/user/messages">消息</a></li>
                <li><a href="/user/markdown">创作</a></li>
                <li><a href="/my_blog">我的</a></li>
                <li id="login_before"><a href="/user/login">登陆/注册</a></li>
                <li id="login_after"><a href="/user/showUserBaseInfo" id="login_after_name">${session.username}</a></li>
                <li id="login_after2"><a href="#" data-toggle="modal"  data-target="#addUserModal3" class="out_btn-big">退出登陆</a></li>
            </ul>
        </nav>
    </div>

</header>

<%--<!-- 模态框（Modal） -->--%>
<%--<div class="modal" id="modal-one" aria-hidden="true">--%>
<%--    <div class="modal-dialog">--%>
<%--        <div class="modal-header">--%>
<%--            <h2>CIT博客提醒您</h2>--%>
<%--            <a href="#" class="out_btn-close" aria-hidden="true">×</a>--%>
<%--        </div>--%>
<%--        <div class="modal-body">--%>
<%--            <p>是否确认退出？一旦退出就需要重新登陆</p>--%>
<%--        </div>--%>
<%--        <div class="modal-footer">--%>
<%--            <a href="user/outLogin" class="out_btn">确 定</a>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--</div>--%>


<article>
    <h1 class="t_nav"><span>像“草根”一样，紧贴着地面，低调的存在，冬去春来，枯荣无恙。</span><a href="/user/showUserBaseInfo" class="n1">用户中心</a><a href="/user/userSet" class="n2">账户管理</a></h1>
    <div class="ab_box">
        <div class="avatar"> <img src="../images/avatar.jpg" style="float:left;"> </div>
        <div class="myblog-head" style="color: #000;font-weight: bold;font-size: 24px" id="name1"> 昵称</div>
        <div class="collection-line"></div>
        <div style="width: 400px;background-color: #f0f7f6;">
            <div style="border:1px solid #dddddd;padding:1%;">
                <form class="layui-form" action="/user">
                    <div class="layui-form-item">
                        <label class="layui-form-label">输入框</label>
                        <div class="layui-input-block">
                            <input type="text" name="title" required  lay-verify="required" placeholder="请输入昵称"  class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">用户 ID</label>
                        <div class="layui-input-block">
                            <input type="text" name="id" required  lay-verify="required"  class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">用户性别</label>
                        <div class="layui-input-block">
                            <input type="radio" name="sex" value="男" title="男">
                            <input type="radio" name="sex" value="女" title="女" checked>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">用户年龄</label>
                        <div class="layui-input-block">
                            <input type="text" name="age"   class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">所在地址</label>
                        <div class="layui-input-inline">
                            <select name="quiz1">
                                <option value="">请选择省</option>
                                <option value="浙江" selected="">浙江省</option>
                                <option value="你的工号">江西省</option>
                                <option value="你最喜欢的老师">福建省</option>
                            </select>
                        </div>
                        <div class="layui-input-inline">
                            <select name="quiz2">
                                <option value="">请选择市</option>
                                <option value="杭州">杭州</option>
                                <option value="宁波" disabled="">宁波</option>
                                <option value="温州">温州</option>
                                <option value="温州">台州</option>
                                <option value="温州">绍兴</option>
                            </select>
                        </div>
                    </div>

                    <div class="layui-inline">
                        <label class="layui-form-label">生日</label>
                        <div class="layui-input-inline">
                            <input type="text" name="date" id="date" lay-verify="date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input">
                        </div>
                    </div>

                    <div class="layui-form-item layui-form-text" style="margin-top: 20px;">
                        <label class="layui-form-label">个人简介</label>
                        <div class="layui-input-block">
                            <textarea name="desc" placeholder="不超过100字" class="layui-textarea"></textarea>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
        <div class="userpage-info-main">



        </div>
        <div class="userpage-info-main">
            <h2>教育信息<a class="a-style" style="float: right;margin-right: 15px;font-size: 18px" data-toggle="modal"  data-target="#addUserModal2">编辑</a></h2>
            <div class="userpage-info"><a class="userpage-info-a1">学校/公司&nbsp;   &nbsp;    </a><a class="userpage-info-a2" id="sch">湖南理工大学</a></div>
            <div class="userpage-info"><a class="userpage-info-a1">专业/职业&nbsp;   &nbsp;    </a><a class="userpage-info-a2" id="pro">软件工程</a></div>
            <div class="userpage-info"><a class="userpage-info-a1">学历/经历&nbsp;   &nbsp;    </a><a class="userpage-info-a2" id="edu">本科</a></div>
        </div>
        <div style="margin-top: 500px"></div>
    </div>
</article>




<footer>
    <p>Design by <a href="/">布拉猫设计</a> <a href="/">蜀ICP备11002373号-1</a></p>
</footer>
<script>
    layui.use(['form', 'layedit', 'laydate'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;

        //日期
        laydate.render({
            elem: '#date'
        });
        laydate.render({
            elem: '#date1'
        });

        //创建一个编辑器
        var editIndex = layedit.build('LAY_demo_editor');

        //自定义验证规则
        form.verify({
            title: function(value){
                if(value.length < 5){
                    return '标题至少得5个字符啊';
                }
            }
            ,pass: [
                /^[\S]{6,12}$/
                ,'密码必须6到12位，且不能出现空格'
            ]
            ,content: function(value){
                layedit.sync(editIndex);
            }
        });

        //监听指定开关
        form.on('switch(switchTest)', function(data){
            layer.msg('开关checked：'+ (this.checked ? 'true' : 'false'), {
                offset: '6px'
            });
            layer.tips('温馨提示：请注意开关状态的文字可以随意定义，而不仅仅是ON|OFF', data.othis)
        });

        //监听提交
        form.on('submit(formDemo)', function(data){
            layer.msg(JSON.stringify(data.field));
            return false;
        });

        //表单赋值
        layui.$('#LAY-component-form-setval').on('click', function(){
            form.val('example', {
                "username": "贤心" // "name": "value"
                ,"password": "123456"
                ,"interest": 1
                ,"like[write]": true //复选框选中状态
                ,"close": true //开关状态
                ,"sex": "女"
                ,"desc": "我爱 layui"
            });
        });

        //表单取值
        layui.$('#LAY-component-form-getval').on('click', function(){
            var data = form.val('example');
            alert(JSON.stringify(data));
        });

    });
</script>

<script src="../js/nav.js"></script>
<script>
</script>
</body>
</html>
