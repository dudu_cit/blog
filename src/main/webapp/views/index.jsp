<%@ page language="java" contentType="text/html;iso-8859-1" import="java.util.*" 
    pageEncoding="utf-8" isELIgnored="false" %>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>CIT博客</title>
<meta name="keywords" content="CIT博客" />
<meta name="description" content="CIT博客" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="../css/base.css" rel="stylesheet">
<link href="../css/index.css" rel="stylesheet">
<link href="../css/m.css" rel="stylesheet">
<link href="../css/model.css" rel="stylesheet">
  <link href="../css/index-xx.css" rel="stylesheet">
<script src="../js/common.js"></script>


<script src="../js/jquery-2.1.1.min.js"></script>

<script>

  //刷新页面一次,更改 url
  $(document).ready(function () {
    var href = "http://localhost:8011/admin/index";
    if(location.href !== href){
      location.href=href;
    }
  });

  //判断session 是否存在
  var session = "${session.id}";
  <%--alert("userEdu_session: "+${userEdu_session});--%>
  $(function () {
    if(session.length ===0 ){
      document.getElementById("login_after").style.display="none";
      document.getElementById("login_after2").style.display="none";
      //alert("未登录用户");
    }else{
      document.getElementById("login_before").style.display="none";
      document.getElementById("login_after_name").innerHTML = "${session.username}";
      //alert("已经登陆"+"${session.username}");
    }
  });


window.onload = function ()
{
	var oH2 = document.getElementsByTagName("h2")[0];
	var oUl = document.getElementsByTagName("ul")[0];
	oH2.onclick = function ()
	{
		var style = oUl.style;
		style.display = style.display == "block" ? "none" : "block";
		oH2.className = style.display == "block" ? "open" : ""
	}
}
</script>
<style>
  .bloginfo-test{
    white-space:nowrap;
    /*width:50em;*/
    height: 30px;
    overflow:hidden;
    text-overflow:ellipsis;
  }
  .bloginfo-test2{
    white-space:nowrap;
    /*width:50em;*/
    height: 60px;
    overflow:hidden;
    text-overflow:ellipsis;
  }


</style>

</head>
<body>
<header>
  <div class="tophead">
    <div class="logo"><a href="/">CIT 博客</a></div>
    <nav class="topnav" id="topnav">
      <ul>
        <li><a href="/admin/index">首页</a></li>
        <li><a href="/admin/blog">博客</a></li>
        <li><a href="/user/collection">收藏</a></li>
        <li><a href="/user/info">动态</a></li>
        <li><a href="/user/messages">消息</a></li>
        <li><a href="/user/markdown">创作</a></li>
        <li><a href="/my_blog">我的</a></li>
        <li id="login_before"><a href="/user/login">登陆/注册</a></li>
        <li id="login_after"><a href="/user/showUserBaseInfo" id="login_after_name">DUDU</a></li>
        <li id="login_after2"><a href="#modal-one" class="out_btn-big">退出登陆</a></li>
      </ul>
    </nav>
  </div>


</header>

<!-- 模态框（Modal） -->
<div class="modal" id="modal-one" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-header">
      <h2>CIT博客提醒您</h2>
      <a href="#" class="out_btn-close" aria-hidden="true">×</a>
    </div>
    <div class="modal-body">
      <p>是否确认退出？一旦退出就需要重新登陆</p>
    </div>
    <div class="modal-footer">
      <a class="out_btn" href="/user/outLogin">确 认</a>
  </div>
  </div>
</div>

<%-- 1. 博客首推 --%>
<div class="picshow">
  <ul>
    <c:forEach  items="${AllBlog}"  var="userItem"  end="4" varStatus="userStatus">
      <li>
        <a href="/article/blog_content?aid=${userItem.article_id}" target="_blank"><i><img src="../images/b01.jpg"></i>
          <div class="font">
            <h3 class="bloginfo-test">${userItem.article_title}</h3>
          </div>
        </a>
      </li>
    </c:forEach>
  </ul>
</div>
<article>

  <!-- 2. 博客 列表展示 -->
  <div class="blogs">
    <c:forEach  items="${AllBlog}"  var="userItem"  end="15" varStatus="userStatus">
      <div class="myblog-body-content">
        <li> <span class="blogpic"><a href="/"><img src="../images/zd02.jpg"></a></span>
          <h3 class="blogtitle " style="font-size: 18px;font-weight: bold"><a href="/article/blog_content?aid=${userItem.article_id}" class="a-style" target="_blank">${userItem.article_title}</a></h3>
          <div class="bloginfo-test">
              ${userItem.article_summary}
          </div>
          <div class="autor">
                        <span class="lm">
                            <a href="/" title="CSS3|Html5" target="_blank" class="classname">${userItem.article_type_name}</a> |
                            <a href="/" title="CSS3|Html5" target="_blank" class="classname">${userItem.article_status_name}</a>
                        </span>
            <span class="dtime">${userItem.article_create_time}</span>
            <span class="viewnum">浏览（<a href="/">${userItem.article_view_count}</a>）</span>
            <span class="dianzan">点赞（<a href="/">${userItem.article_order}</a>）</span>
            <span class="readmore"><a href="/article/blog_content?aid=${userItem.article_id}" target="_blank">阅读原文</a></span>
          </div>
        </li>
      </div>
    </c:forEach>
  </div>


<%--  3. 博客专家--%>
  <div class="sidebar paihang1">
    <div class="about ZhuanJia-head">
      <h2 class="hometitle">博客专家</h2>
      <ul>
        <c:forEach  items="${authorInfoModel}"  var="userItem"  end="5" varStatus="userStatus">
          <li>
            <p>
              <i><img src="../images/avatar.jpg" class="ZhuanJia-img"></i>
              <span class="ZhuanJia-body-order1"><a class="ZhuanJia-body-tt">专家</a><a href="#" class="a-style">${userItem.author_name}</a></span>
              <a class="ZhuanJia-body-order2">No.${userStatus.count}</a>
            </p>
          </li>
        </c:forEach>
      </ul>
    </div>


<%--    4. 搜索框--%>
    <div class="search">
      <form action="/e/search/index.php" method="post" name="searchform" id="searchform">
        <input name="keyboard" id="keyboard" class="input_text" value="请输入关键字" style="color: rgb(153, 153, 153);" onfocus="if(value=='请输入关键字'){this.style.color='#000';value=''}" onblur="if(value==''){this.style.color='#999';value='请输入关键字'}" type="text">
        <input name="show" value="title" type="hidden">
        <input name="tempid" value="1" type="hidden">
        <input name="tbname" value="news" type="hidden">
        <input name="Submit" class="input_submit" value="搜索" type="submit">
      </form>
    </div>

<%--    5. 便签云--%>
    <div class="cloud">
      <h2 class="hometitle">标签云</h2>
      <ul>
        <c:forEach  items="${tagModel}"  var="userItem"  varStatus="userStatus">
          <a href="/">${userItem.tag_name}</a>
        </c:forEach>
      </ul>
    </div>

<%--   6.  站长推荐--%>
    <div class="paihang">
      <h2 class="hometitle">站长推荐</h2>
      <ul>
        <c:forEach  items="${authorInfoModel2}"  var="userItem"  end="7" varStatus="userStatus">
          <li><b><a href="/article/blog_content?aid=${userItem.article_id}" target="_blank">${userItem.article_title}</a></b>
            <p class="bloginfo-test2"><i><img src="../images/t02.jpg"></i>${userItem.article_summary}</p>
          </li>
        </c:forEach>
      </ul>
    </div>

<%--    7. 友情链接--%>
    <div class="links">
      <h2 class="hometitle">友情链接</h2>
      <ul>
        <c:forEach  items="${linkModel}"  var="userItem"  end="3" varStatus="userStatus">
          <li><a href="${userItem.link_url}" title="布拉猫设计" class="a-style">${userItem.link_name}</a></li>
        </c:forEach>
      </ul>
    </div>

<%--    10. 官方微信--%>
    <div class="weixin">
      <h2 class="hometitle">官方微信</h2>
      <ul>
        <img src="../images/wx.jpg">
      </ul>
    </div>
  </div>

</article>

<%--尾部--%>
<div class="blank"></div>
<footer>
  <p>@DUDU <a href="/">CIT博客</a> <a href="/">蜀ICP备11002373号-1**</a></p>
</footer>
<script src="../js/nav.js"></script>
</body>
</html>
