<%--
  Created by IntelliJ IDEA.
  User: dudu
  Date: 2021/3/12
  Time: 23:08
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html;iso-8859-1" import="java.util.*"
         pageEncoding="utf-8" isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>CIT博客注册</title>
    <meta name="keywords" content="CIT博客" />
    <meta name="description" content="CIT博客" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/base.css" rel="stylesheet">
    <link href="../css/index.css" rel="stylesheet">
    <link href="../css/m.css" rel="stylesheet">
    <link href="../css/model.css" rel="stylesheet">
    <script type="text/javascript" src="../js/jquery-2.1.1.min.js"></script>
    <!--    <link rel="stylesheet" href="login2/css/reset.min.css">-->
    <link rel="stylesheet" href="../login2/css/style.css">

    <script src="js/modernizr.js"></script>

    <script type="text/javascript">

        //判断session 是否存在

        var session = "${session.id}";

        // alert(session);
        $(function () {
            if(session.length ===0 ){
                document.getElementById("login_after").style.display="none";
                document.getElementById("login_after2").style.display="none";
                //alert("未登录用户");
            }else{
                document.getElementById("login_before").style.display="none";
                document.getElementById("login_after_name").innerHTML = "${session.username}";
                ///alert("已经登陆"+"${session.username}");
            }
        });

        window.onload = function ()
        {
            var oH2 = document.getElementsByTagName("h2")[0];
            var oUl = document.getElementsByTagName("ul")[0];
            oH2.onclick = function ()
            {
                var style = oUl.style;
                style.display = style.display == "block" ? "none" : "block";
                oH2.className = style.display == "block" ? "open" : ""
            }
        };

        /**
         * 注册判断
         */

        // var username = document.getElementById("username").value;
        // var password1 = document.getElementById("password1").value;
        // var password2 = document.getElementById("password2").value;
        // var phone = document.getElementById("phone").value;
        // var email  = document.getElementById("email").value;



        //输入第二个密码时进行判断是否正确
        function pwdChange(){
            var password1 = document.getElementById('password1').value;
            var password2 = document.getElementById("password2").value;
            if(password1.length === 0 ){
                alert("请先输入密码");
                //清空确认密码输入框
                document.getElementById("password2").value="";
            }else{
                if(password1 !== password2){
                    alert("两次输入的密码不相同");
                    document.getElementById("password1").value="";
                    document.getElementById("password2").value="";
                }
            }
        }

        // //判断输入的是否为电话号码
        // function phChange(){
        //     var phone = document.getElementById("phone").value;
        //     var reg=/^((0\d{2,3})-)?(\d{7,8})(-(\d{3,}))?$/;
        //     if(!reg.test(phone)){
        //         alert("请填写正确的电话号码");
        //         document.getElementById("phone").value="";
        //     }
        // }

        //判断邮箱格式是否正确
        function emChange(){
            var email  = document.getElementById("email").value;
            var emreg=/^\w{3,}(\.\w+)*@[A-z0-9]+(\.[A-z]{2,5}){1,2}$/;

            if(emreg.test(email) == false){
                alert("该邮箱不合法");
                document.getElementById("email").value="";
            }
        }

        //判断该用户是否已经注册
        function UserChange(){
            var username = document.getElementById("username").value;
            $.ajax({
                type:"post",
                url:"/user/provingUsername",//服务器端请求地址
                async:true,//true为异步
                data:{"username":username},//传递的数据
                dataType:"json",//告诉服务器，我想要什么类型的数据
                //contentType:"application/json;charset=UTF-8",//告诉服务器我要发送什么类型的数据
                success:function(data){//成功时处理函数，data为Controller层返回的数据
                    if(data.code == 1){
                        alert("该账号已经注册过啦");
                        document.getElementById("username").value = "";
                    }
                },
                error:function(){
                    window.location.href = "error";
                }
            });
        }

        $(function () {
            $("#registe_btn").click(function () {
                //alert("u:99");
                var username = document.getElementById("username").value;
                var password1 = document.getElementById("password1").value;
                var password2 = document.getElementById("password2").value;
                var phone = document.getElementById("phone").value;
                var email = document.getElementById("email").value;
                //alert("u:99");

                if(username.length === 0 || password1.length === 0){
                    alert("账号或密码为空");
                    //清空输入框
                    // document.getElementById("username").innerText = "";
                    // document.getElementById("password").innerText = "";
                }else if(password2.length === 0){
                    alert("请再一次输入密码");
                }else {
                    var myDate = new Date();
                    var createtime = myDate.toLocaleString( );
                    //alert("createtime: "+createtime);
                    $.ajax({
                        async:false,//同步，请求
                        url:"/user/userRigistrate",
                        contentType:"application/json;charset=utf-8",
                        data:JSON.stringify({username:username,password:password1,phone:phone,email:email,createtime:createtime}),
                        type:"post",
                        dataType:"json",
                        success:function (data) {
                            alert(data);
                            window.location.href = "/user/login";
                        },
                        error:function (xhr) { //连接失败 xhr为失败的信息
                            //alert(xhr);
                            window.location.href = "/user/login";
                        }
                    });
                }
            });
        });

    </script>
    <style>


    </style>
</head>
<body>
<header>
    <div class="tophead">
        <div class="logo"><a href="/">CIT 博客</a></div>
        <nav class="topnav" id="topnav">
            <ul>
                <li><a href="/admin/index">首页</a></li>
                <li><a href="/admin/blog">博客</a></li>
                <li><a href="/user/collection">收藏</a></li>
                <li><a href="/user/info">动态</a></li>
                <li><a href="/user/messages">消息</a></li>
                <li><a href="/user/markdown">创作</a></li>
                <li><a href="/my_blog">我的</a></li>
                <li id="login_before"><a href="/user/login">登陆/注册</a></li>
                <li id="login_after"><a href="/user/showUserBaseInfo" id="login_after_name">DUDU</a></li>
                <li id="login_after2"><a href="#modal-one" class="out_btn-big">退出登陆</a></li>
            </ul>
        </nav>
    </div>
</header>


<!-- 模态框（Modal） -->
<div class="modal" id="modal-one" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-header">
            <h2>CIT博客提醒您</h2>
            <a href="#" class="out_btn-close" aria-hidden="true">×</a>
        </div>
        <div class="modal-body">
            <p>是否确认退出？一旦退出就需要重新登陆</p>
        </div>
        <div class="modal-footer">
            <a href="/user/outLogin" class="out_btn">确 定</a>
        </div>
    </div>
</div>


<article>
    <h1 class="t_nav"></h1>
    <div class="ab_box">
        <h1 class="t_nav"><span>像“草根”一样，紧贴着地面，低调的存在，冬去春来，枯荣无恙。</span><a href="/user/login" class="n2">登陆</a><a href="/user/registration" class="n1">注册</a></h1>

        <div class="container">
            <div class="panel">
                <div class="panel-header">
                    <div class="panel-header-logo">
                        <img src="../images/bi04.jpg" width="126" height="38" />
                    </div>
                    <h1 class="panel-header-title">注册 CIT博客</h1>
                </div>
                <form class="form" action="javascript:void(0);">
                    <div class="input">
                        <label class="input-label">用&thinsp; 户&thinsp; 名</label>
                        <input type="text" class="input-field" name="username" id="username" onchange="UserChange()"/>
                    </div>
                    <div class="input">
                        <label class="input-label">设置密码</label>
                        <input type="password" class="input-field" name="password1" id="password1"/>
                    </div>
                    <div class="input">
                        <label class="input-label">确认密码</label>
                        <input type="password" class="input-field" name="password2" id="password2" onchange="pwdChange()"/>
                    </div>
                    <div class="input">
                        <label class="input-label">手&thinsp; 机&thinsp; 号<a style="color: red"> &emsp;&emsp;&emsp;非必填</a></label>
                        <input type="password" class="input-field" name="phone" id="phone" onchange="phChange()"/>
                    </div><div class="input">
                    <label class="input-label">邮&emsp;&emsp;箱<a style="color: red"> &emsp;&emsp;&emsp;非必填</a></label>
                    <input type="password" class="input-field" name="email" id="email" onchange="emChange()"/>
                </div>
                    <div class="input">
                        <button class="input-submit" id="registe_btn">立即注册</button>
                    </div>
                </form>
            </div>
        </div>

        <div style="margin-top: 100px"></div>
    </div>


</article>

<footer>
    <p>@DUDU <a href="/">CIT博客</a> <a href="/">蜀ICP备11002373号-1</a></p>
</footer>


</body>
</html>
