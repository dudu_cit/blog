package com.cm.dao;

import com.cm.model.LikeModel;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * 点赞表 数据库操作
 */
@Repository
public interface LikeDao {

    /**
     * 查询 某个用户是否为 某个文章点过赞
     * @param like_user_id
     * @param like_article_id
     * @return
     */
    @Select("select * from Alike where like_user_id=#{like_user_id} and like_article_id=#{like_article_id}")
    public LikeModel selectLikeById(@Param("like_user_id")int like_user_id, @Param("like_article_id")int like_article_id);

    /**
     * 取消 点赞
     * @param like_user_id
     * @param like_article_id
     * @return
     */
    @Delete("delete from Alike where like_user_id=#{like_user_id} and like_article_id=#{like_article_id}")
    public int deleteLike(@Param("like_user_id")int like_user_id, @Param("like_article_id")int like_article_id);

    /**
     * 点赞 增加一条数据
     * @param like_user_id
     * @param like_article_id
     * @return
     */
    @Insert("insert into Alike(like_user_id,like_article_id) value(#{like_user_id},#{like_article_id})")
    public int insertLike(@Param("like_user_id")int like_user_id, @Param("like_article_id")int like_article_id);

}
