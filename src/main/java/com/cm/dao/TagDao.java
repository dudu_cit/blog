package com.cm.dao;

import com.cm.model.TagModel;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 标签云 数据库操作类
 */
@Repository
public interface TagDao  {

    /**
     * 查询所有tag
     * @return
     */
    @Select("select * from tag")
    public List<TagModel> selectTag();
}
