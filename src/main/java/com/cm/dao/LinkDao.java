package com.cm.dao;

import com.cm.model.LinkModel;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 友情链接数据库操作
 */
@Repository
public interface LinkDao {

    /**
     * 查询所有的link
     * @return
     */
    @Select("select * from link")
    public List<LinkModel> selectLink();

}
