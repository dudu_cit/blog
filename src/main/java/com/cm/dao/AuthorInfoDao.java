package com.cm.dao;

import com.cm.model.AuthorInfoModel;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 作者基本信息数据库操类，操作，排名，博客数量，粉丝数量，访问量，点赞数量
 */
@Repository
public interface AuthorInfoDao {


    /**
     * 查询作者的基本信息：排名，博客数量，粉丝数量，访问量，点赞数量
     * @return
     */
    @Select("SELECT uid author_id,NAME author_name,COUNT(article_user_id) blog_number,SUM(article_view_count) access_number,SUM(article_like_count) like_number " +
            "FROM user_info,article " +
            "WHERE article_user_id=uid GROUP BY article_user_id ORDER BY COUNT(article_user_id) DESC")
    public List<AuthorInfoModel> selectAuthorInfo();

    /**
     * 查询粉丝的数量
     * @param id
     * @return
     */
    @Select("SELECT COUNT(concern_fax_id) fax_number FROM concern WHERE concern_fax_id=#{id}")
    public int SelectFaxNumber(int id);

}
