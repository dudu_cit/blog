package com.cm.dao;

import com.cm.model.CollJiaModel;
import com.cm.model.CollectionModel;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 收藏 数据亏操作
 */
@Repository
public interface CollectionDao {

    /**
     * 查询用户创建 的收藏夹
     * @param uid
     * @return
     */
    @Select("select * from CJia where CJia_user_id=#{uid}")
    public List<CollJiaModel> selectCJia(int uid);

    /**
     * 增加收藏夹
     * @param
     */
    @Insert("insert into CJia(CJia_name,CJia_user_id) value(#{a},#{b})")
    public void insertCJia(@Param("a")String a,@Param("b")int b);

    /**
     * 删除收藏夹
     * @param CJia_id
     * @param CJia_user_id
     */
    @Delete("delete from CJia where CJia_id=#{CJia_id} and CJia_user_id=#{CJia_user_id}")
    public int deleteCJia(@Param("CJia_id")int CJia_id,@Param("CJia_user_id")int CJia_user_id);


    /**
     * 查询 某个用户、某个收藏夹中中的内容
     * @param collection_user_id
     * @param collection_jia
     * @return
     */
    @Select("select collection_user_id,collection_article_id,collection_jia,article_title " +
            "from collection,article " +
            "where collection_user_id=#{collection_user_id} and collection_jia=#{collection_jia} and collection_article_id=article_id")
    public List<CollectionModel> selectCollection(@Param("collection_user_id")int collection_user_id, @Param("collection_jia")int collection_jia);

    /**
     * 取消收藏
     * @param collection_user_id
     * @param collection_article_id
     */
    @Delete("delete from collection where collection_user_id=#{collection_user_id} and collection_article_id=#{collection_article_id}")
    public int deleteCollection(@Param("collection_user_id")int collection_user_id,@Param("collection_article_id")int collection_article_id);

    /**
     * 查询某个用户、是否收藏过某个文章
     * @param collection_user_id
     * @param collection_article_id
     * @return
     */
    @Select("select * from collection where collection_user_id=#{collection_user_id} and collection_article_id=#{collection_article_id}")
    public CollectionModel selectCollectionById(@Param("collection_user_id")int collection_user_id,@Param("collection_article_id")int collection_article_id);

    /**
     * 添加 收藏 到 收藏夹
     * @param collection_user_id
     * @param collection_article_id
     * @param collection_jia
     * @return
     */
    @Insert("insert into collection(collection_user_id,collection_article_id,collection_jia) value(#{collection_user_id},#{collection_article_id},#{collection_jia}) ")
    public int insertCollection(@Param("collection_user_id")int collection_user_id,@Param("collection_article_id")int collection_article_id,@Param("collection_jia")int collection_jia);

    /**
     * 根据收藏夹、用户名删除 收藏，，，删除收藏夹中的所有内容
     * @param collection_user_id
     * @param collection_jia
     * @return
     */
    @Delete("delete from collection where collection_user_id=#{collection_user_id} and collection_jia=#{collection_jia}")
    public int deleteCollectionByCJIa(@Param("collection_user_id")int collection_user_id,@Param("collection_jia")int collection_jia);

}
