package com.cm.dao;

import com.cm.model.ArticleModel;
import com.cm.model.ArticleTypeModel;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 文章数据库操作
 */
@Repository
public interface ArticleDao {

    /**
     * 增加文章
     * Options 返回新增的文章id
     * @param articleModel
     */
    @Insert("insert into article(article_user_id,article_title,article_content,article_is_comment,article_status,article_create_time,article_summary) " +
            "value(#{article_user_id},#{article_title},#{article_content},#{article_is_comment},#{article_status},#{article_create_time},#{article_summary})")
    @Options(useGeneratedKeys=true, keyProperty="article_id", keyColumn="article_id")
    public void insertArticle(ArticleModel articleModel);

    /**
     * 添加文章类型映射
     * @param articleTypeModel
     */
    @Insert("insert into article_type(article_id,article_type_id) value(#{article_id},#{article_type_id})")
    public void insertArticleType(ArticleTypeModel articleTypeModel);

    /**
     * 查询所有文章，集合以供 显示 和 模糊查询
     * @return
     */
    @Select("select * from article")
    public List<ArticleModel> selectArticle();

    /**
     * 根据文章的id查询文章的内容
     * @param article_id
     * @return
     */
    @Select("select * from article where article_id=#{article_id}")
    public ArticleModel selectArticleById(int article_id);

    /**
     * 根据 文章的id 查询 文章的标签id
     * @param article_id
     * @return
     */
    @Select("select article_type_id from article_type where article_id=#{article_id}")
    public int selectArticleTypeId(int article_id);

    /**
     * 根据 文章的id 查询 文章的标签id
     * @param tag_id
     * @return
     */
    @Select("select tag_name from tag where tag_id=#{tag_id}")
    public String selectArticleType(int tag_id);


    /**
     * 更新文章
     * @param articleModel
     */
    @Update("update article set article_title=#{article_title},article_content=#{article_content},article_view_count=#{article_view_count}" +
            ",article_comment_count=#{article_comment_count},article_like_count=#{article_like_count},article_is_comment=#{article_is_comment}" +
            ",article_status=#{article_status},article_order=#{article_order},article_update_time=#{article_update_time}" +
            ",article_create_time=#{article_create_time} where article_id=#{article_id}")
    public void updateArticle(ArticleModel articleModel);

    /**
     * 查询 当前用户的所有文章
     * @param article_user_id
     * @return
     */
    @Select("select * from article where article_user_id=#{article_user_id}")
    public List<ArticleModel> selectArticleByUserId(int article_user_id);

    /**
     * 根据文章 id 删除文
     * @param article_id
     */
    @Delete("delete from article where article_id=#{article_id}")
    public void deleteArticleById(int article_id);

    /**
     * 按 排名 查询 文章的 id
     * @return
     */
    @Select("select * from article ORDER BY article_view_count DESC")
    public List<ArticleModel> getArticleOrder();

    /**
     * 更新文章的点赞个数
     * @param article_like_count
     * @param article_id
     * @return
     */
    @Update("update article set article_like_count=#{article_like_count} where article_id=#{article_id}")
    public int updateArticleLike(@Param("article_like_count")int article_like_count,@Param("article_id")int article_id);




}
