package com.cm.dao;

import com.cm.model.UserEduModel;
import com.cm.model.UserInfoModel;
import com.cm.model.UserModel;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao {

    // 查询所有用户账号信息
    @Select("select * from user")
    public List<UserModel>  selectAllUserInfo();

    //根据用户名查询账号数据
    @Select("select * from user where username=#{username}")
    public UserModel selectUserByName(String username);

    //插入用户账号数据
    @Insert("insert into user(username,password,phone,email,createtime) value(#{username},#{password},#{phone},#{email},#{createtime})")
    public void insertUser(UserModel userModel);

    //更新用户账号数据
    @Update("update user set password=#{password},phone=#{phone},email=#{email} where id=#{id}")
    public void updateUser(UserModel userModel);

    //更新用户基本信息
    @Update("update user_info set name=#{name},sex=#{sex},age=#{age},introduction=#{introduction},address=#{address},birthday=#{birthday} where uid=#{uid}")
    public void updateUserBaseInfo(UserInfoModel userInfoModel);

    //查询用户基本信息
    @Select("select * from user_info where uid=#{id}")
    public UserInfoModel selectUserBaseInfo(int id);

    //插入用户基本信息表
    @Insert("insert into user_info(uid,name,sex,age,introduction,address,birthday) value(#{uid},#{name},#{sex},#{age},#{introduction},#{address},#{birthday})")
    public void insertUserBaseInfo(UserInfoModel userInfoModel);

    //查询用户教育信息
    @Select("select * from user_edu where uid=#{id}")
    public UserEduModel selectUserEduInfo(int id);

    //更新用户教育信息
    @Update("update user_edu set sch=#{sch},pro=#{pro},edu=#{edu} where uid=#{uid}")
    public void updateUserEduInfo(UserEduModel userEduModel);

    //插入用户教育信息
    @Insert("insert into user_edu(uid,sch,pro,edu) value(#{uid},#{sch},#{pro},#{edu})")
    public void insertUserEduInfo(UserEduModel userEduModel);


}
