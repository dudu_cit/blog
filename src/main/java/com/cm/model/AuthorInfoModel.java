package com.cm.model;

import java.io.Serializable;

/**
 * 作者信息实体类
 */
public class AuthorInfoModel implements Serializable {

    private int author_id;
    private String author_name;//作者昵称
    private int author_order;//作者排名
    private int blog_number;//作者文章数量
    private int access_number;//作者访客数量
    private int like_number;//作者点赞数量
    private int fax_number;//作者粉丝数量

    public AuthorInfoModel(){}

    public AuthorInfoModel(int author_id, String author_name, int author_order, int blog_number, int access_number, int like_number, int fax_number) {
        this.author_id = author_id;
        this.author_name = author_name;
        this.author_order = author_order;
        this.blog_number = blog_number;
        this.access_number = access_number;
        this.like_number = like_number;
        this.fax_number = fax_number;
    }

    public int getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(int author_id) {
        this.author_id = author_id;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public int getAuthor_order() {
        return author_order;
    }

    public void setAuthor_order(int author_order) {
        this.author_order = author_order;
    }

    public int getBlog_number() {
        return blog_number;
    }

    public void setBlog_number(int blog_number) {
        this.blog_number = blog_number;
    }

    public int getAccess_number() {
        return access_number;
    }

    public void setAccess_number(int access_number) {
        this.access_number = access_number;
    }

    public int getLike_number() {
        return like_number;
    }

    public void setLike_number(int like_number) {
        this.like_number = like_number;
    }

    public int getFax_number() {
        return fax_number;
    }

    public void setFax_number(int fax_number) {
        this.fax_number = fax_number;
    }

    @Override
    public String toString() {
        return "AuthorInfoModel{" +
                "author_id=" + author_id +
                ", author_name='" + author_name + '\'' +
                ", author_order=" + author_order +
                ", blog_number=" + blog_number +
                ", access_number=" + access_number +
                ", like_number=" + like_number +
                ", fax_number=" + fax_number +
                '}';
    }
}
