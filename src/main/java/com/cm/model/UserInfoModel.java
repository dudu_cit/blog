package com.cm.model;

import java.io.Serializable;

/**
 * 用户基本信息 实体类
 */
public class UserInfoModel implements Serializable {

    private int id,uid;
    private String name;
    private String sex;
    private int age;
    private String introduction;
    private String address;
    private String birthday;

    public UserInfoModel(){}

    public UserInfoModel(int id, int uid, String name, String sex, int age, String introduction, String address, String birthday) {
        this.id = id;
        this.uid = uid;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.introduction = introduction;
        this.address = address;
        this.birthday = birthday;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "UserInfoModel{" +
                "id=" + id +
                ", uid=" + uid +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", introduction='" + introduction + '\'' +
                ", address='" + address + '\'' +
                ", birthday='" + birthday + '\'' +
                '}';
    }
}
