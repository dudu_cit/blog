package com.cm.model;

import java.io.Serializable;

/**
 * 文章 实体类
 */
public class ArticleModel implements Serializable {

    private int article_id;
    private int article_user_id;
    private String article_title;
    private String article_content;
    private int article_view_count;
    private int article_comment_count;
    private int article_like_count;
    private int article_is_comment;//1：公布，0：草稿
    private int article_status;//1：原创，0：转载
    private int article_order;//点赞
    private String article_update_time;
    private String article_create_time;
    private String article_summary;
    private int article_type_id;

//    private ArticleTypeModel articleTypeModel;//级联 类型表
    private String article_type_name;//文章的标签
    private String article_status_name;//1：原创，0：转载
    public ArticleModel(){}

    public ArticleModel(int article_id, int article_user_id, String article_title, String article_content, int article_view_count, int article_comment_count, int article_like_count, int article_is_comment, int article_status, int article_order, String article_update_time, String article_create_time, String article_summary, int article_type_id, String article_type_name, String article_status_name) {
        this.article_id = article_id;
        this.article_user_id = article_user_id;
        this.article_title = article_title;
        this.article_content = article_content;
        this.article_view_count = article_view_count;
        this.article_comment_count = article_comment_count;
        this.article_like_count = article_like_count;
        this.article_is_comment = article_is_comment;
        this.article_status = article_status;
        this.article_order = article_order;
        this.article_update_time = article_update_time;
        this.article_create_time = article_create_time;
        this.article_summary = article_summary;
        this.article_type_id = article_type_id;
        this.article_type_name = article_type_name;
        this.article_status_name = article_status_name;
    }

    public int getArticle_id() {
        return article_id;
    }

    public void setArticle_id(int article_id) {
        this.article_id = article_id;
    }

    public int getArticle_user_id() {
        return article_user_id;
    }

    public void setArticle_user_id(int article_user_id) {
        this.article_user_id = article_user_id;
    }

    public String getArticle_title() {
        return article_title;
    }

    public void setArticle_title(String article_title) {
        this.article_title = article_title;
    }

    public String getArticle_content() {
        return article_content;
    }

    public void setArticle_content(String article_content) {
        this.article_content = article_content;
    }

    public int getArticle_view_count() {
        return article_view_count;
    }

    public void setArticle_view_count(int article_view_count) {
        this.article_view_count = article_view_count;
    }

    public int getArticle_comment_count() {
        return article_comment_count;
    }

    public void setArticle_comment_count(int article_comment_count) {
        this.article_comment_count = article_comment_count;
    }

    public int getArticle_like_count() {
        return article_like_count;
    }

    public void setArticle_like_count(int article_like_count) {
        this.article_like_count = article_like_count;
    }

    public int getArticle_is_comment() {
        return article_is_comment;
    }

    public void setArticle_is_comment(int article_is_comment) {
        this.article_is_comment = article_is_comment;
    }

    public int getArticle_status() {
        return article_status;
    }

    public void setArticle_status(int article_status) {
        this.article_status = article_status;
    }

    public int getArticle_order() {
        return article_order;
    }

    public void setArticle_order(int article_order) {
        this.article_order = article_order;
    }

    public String getArticle_update_time() {
        return article_update_time;
    }

    public void setArticle_update_time(String article_update_time) {
        this.article_update_time = article_update_time;
    }

    public String getArticle_create_time() {
        return article_create_time;
    }

    public void setArticle_create_time(String article_create_time) {
        this.article_create_time = article_create_time;
    }

    public String getArticle_summary() {
        return article_summary;
    }

    public void setArticle_summary(String article_summary) {
        this.article_summary = article_summary;
    }

    public int getArticle_type_id() {
        return article_type_id;
    }

    public void setArticle_type_id(int article_type_id) {
        this.article_type_id = article_type_id;
    }

    public String getArticle_type_name() {
        return article_type_name;
    }

    public void setArticle_type_name(String article_type_name) {
        this.article_type_name = article_type_name;
    }

    public String getArticle_status_name() {
        return article_status_name;
    }

    public void setArticle_status_name(String article_status_name) {
        this.article_status_name = article_status_name;
    }

    @Override
    public String toString() {
        return "ArticleModel{" +
                "article_id=" + article_id +
                ", article_user_id=" + article_user_id +
                ", article_title='" + article_title + '\'' +
                ", article_content='" + article_content + '\'' +
                ", article_view_count=" + article_view_count +
                ", article_comment_count=" + article_comment_count +
                ", article_like_count=" + article_like_count +
                ", article_is_comment=" + article_is_comment +
                ", article_status=" + article_status +
                ", article_order=" + article_order +
                ", article_update_time='" + article_update_time + '\'' +
                ", article_create_time='" + article_create_time + '\'' +
                ", article_summary='" + article_summary + '\'' +
                ", article_type_id=" + article_type_id +
                ", article_type_name='" + article_type_name + '\'' +
                ", article_status_name='" + article_status_name + '\'' +
                '}';
    }
}
