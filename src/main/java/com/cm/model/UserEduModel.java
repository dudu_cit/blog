package com.cm.model;

import java.io.Serializable;

/**
 * 用户 教育信息 实体类
 */
public class UserEduModel implements Serializable {

    private int id,uid;
    private String sch;
    private String pro;
    private String edu;

    public UserEduModel(){}

    public UserEduModel(int id, int uid, String sch, String pro, String edu) {
        this.id = id;
        this.uid = uid;
        this.sch = sch;
        this.pro = pro;
        this.edu = edu;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getSch() {
        return sch;
    }

    public void setSch(String sch) {
        this.sch = sch;
    }

    public String getPro() {
        return pro;
    }

    public void setPro(String pro) {
        this.pro = pro;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    @Override
    public String toString() {
        return "UserEduModel{" +
                "id=" + id +
                ", uid=" + uid +
                ", sch='" + sch + '\'' +
                ", pro='" + pro + '\'' +
                ", edu='" + edu + '\'' +
                '}';
    }
}
