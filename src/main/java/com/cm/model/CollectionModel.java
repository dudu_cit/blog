package com.cm.model;


import java.io.Serializable;

/**
 * 用户评论表，实体类
 */
public class CollectionModel implements Serializable {

    private int collection_user_id;
    private int collection_article_id;
    private int collection_jia;


    //级联查询
    private String article_title;

    public CollectionModel(){}

    public CollectionModel(int collection_user_id, int collection_article_id, int collection_jia, String article_title) {
        this.collection_user_id = collection_user_id;
        this.collection_article_id = collection_article_id;
        this.collection_jia = collection_jia;
        this.article_title = article_title;
    }

    public int getCollection_user_id() {
        return collection_user_id;
    }

    public void setCollection_user_id(int collection_user_id) {
        this.collection_user_id = collection_user_id;
    }

    public int getCollection_article_id() {
        return collection_article_id;
    }

    public void setCollection_article_id(int collection_article_id) {
        this.collection_article_id = collection_article_id;
    }

    public int getCollection_jia() {
        return collection_jia;
    }

    public void setCollection_jia(int collection_jia) {
        this.collection_jia = collection_jia;
    }

    public String getArticle_title() {
        return article_title;
    }

    public void setArticle_title(String article_title) {
        this.article_title = article_title;
    }

    @Override
    public String toString() {
        return "CollectionModel{" +
                "collection_user_id=" + collection_user_id +
                ", collection_article_id=" + collection_article_id +
                ", collection_jia=" + collection_jia +
                ", article_title='" + article_title + '\'' +
                '}';
    }
}
