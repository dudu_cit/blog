package com.cm.model;

import java.io.Serializable;

public class LinkModel implements Serializable {

    //link_name,link_url,link_ctime
    private int link_id;
    private String link_name;
    private String link_url;
    private String link_ctime;

    public LinkModel(){}

    public LinkModel(int link_id, String link_name, String link_url, String link_ctime) {
        this.link_id = link_id;
        this.link_name = link_name;
        this.link_url = link_url;
        this.link_ctime = link_ctime;
    }

    public int getLink_id() {
        return link_id;
    }

    public void setLink_id(int link_id) {
        this.link_id = link_id;
    }

    public String getLink_name() {
        return link_name;
    }

    public void setLink_name(String link_name) {
        this.link_name = link_name;
    }

    public String getLink_url() {
        return link_url;
    }

    public void setLink_url(String link_url) {
        this.link_url = link_url;
    }

    public String getLink_ctime() {
        return link_ctime;
    }

    public void setLink_ctime(String link_ctime) {
        this.link_ctime = link_ctime;
    }

    @Override
    public String toString() {
        return "LinkModel{" +
                "link_id=" + link_id +
                ", link_name='" + link_name + '\'' +
                ", link_url='" + link_url + '\'' +
                ", link_ctime='" + link_ctime + '\'' +
                '}';
    }
}
