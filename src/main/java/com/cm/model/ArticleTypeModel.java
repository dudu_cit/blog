package com.cm.model;

import java.io.Serializable;

/**
 * 文章类型映射表，实体类
 */
public class ArticleTypeModel implements Serializable {

    private int article_id;
    private int article_type_id;

    public ArticleTypeModel(){}

    public ArticleTypeModel(int article_id, int article_type_id) {
        this.article_id = article_id;
        this.article_type_id = article_type_id;
    }

    public int getArticle_id() {
        return article_id;
    }

    public void setArticle_id(int article_id) {
        this.article_id = article_id;
    }

    public int getArticle_type_id() {
        return article_type_id;
    }

    public void setArticle_type_id(int article_type_id) {
        this.article_type_id = article_type_id;
    }

    @Override
    public String toString() {
        return "ArticleTypeModel{" +
                "article_id=" + article_id +
                ", article_type_id=" + article_type_id +
                '}';
    }
}
