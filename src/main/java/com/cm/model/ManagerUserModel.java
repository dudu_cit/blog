package com.cm.model;

import java.io.Serializable;

/**
 * 管理员账号 实体类
 */
public class ManagerUserModel implements Serializable {

    private int id;//管理员id
    private String username;//管理员账号
    private String password;//管理员密码
    private int status;//管理员级别
    private String createtime;//管理员创建时间

    public ManagerUserModel(){}

    public ManagerUserModel(int id, String username, String password, int status, String createtime) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.status = status;
        this.createtime = createtime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    @Override
    public String toString() {
        return "ManagerUserModel{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", status=" + status +
                ", createtime='" + createtime + '\'' +
                '}';
    }
}
