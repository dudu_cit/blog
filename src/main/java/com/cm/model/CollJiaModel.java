package com.cm.model;

import java.io.Serializable;

public class CollJiaModel implements Serializable {

    private int CJia_id;
    private String CJia_name;
    private int Cjia_user_id;

    public CollJiaModel(){}

    public CollJiaModel(int CJia_id, String CJia_name, int cjia_user_id) {
        this.CJia_id = CJia_id;
        this.CJia_name = CJia_name;
        Cjia_user_id = cjia_user_id;
    }

    public int getCJia_id() {
        return CJia_id;
    }

    public void setCJia_id(int CJia_id) {
        this.CJia_id = CJia_id;
    }

    public String getCJia_name() {
        return CJia_name;
    }

    public void setCJia_name(String CJia_name) {
        this.CJia_name = CJia_name;
    }

    public int getCjia_user_id() {
        return Cjia_user_id;
    }

    public void setCjia_user_id(int cjia_user_id) {
        Cjia_user_id = cjia_user_id;
    }

    @Override
    public String toString() {
        return "CollJiaModel{" +
                "CJia_id=" + CJia_id +
                ", CJia_name='" + CJia_name + '\'' +
                ", Cjia_user_id=" + Cjia_user_id +
                '}';
    }
}
