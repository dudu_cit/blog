package com.cm.common;


import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 自定义 拦截器
 */
public class MyInterceptor implements HandlerInterceptor {

    /**
     * 预处理，拦截方法在controller方法执行之前 执行，设置 请求 的编码格式
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //设置请求编码
        request.setCharacterEncoding("utf-8");
//        //拦截请求，判断用户是否登陆，没有登陆，跳转到登陆界面
//        ModelMap modelMap  = new ModelMap();
//        //如果没有设置session跳转到登陆界面
//        if (modelMap.get("session") == null){
//            request.getRequestDispatcher("/user/login").forward(request,response);
//        }
        System.out.println("拦截器： 预处理执行。。。");
        //
        //放行
        return true;
    }

    /**
     * 后处理，拦截方法在controller方法执行之后 执行，jsp页面执行之前 执行，设置 响应 的编码格式
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        //System.out.println("拦截器： 处理执行。。。");
        //转发 到错误界面
        //request.getRequestDispatcher("/views/error.jsp").forward(request,response);
    }

//    /**
//     * 结束处理，拦截方法在 jsp 执行之后 执行
//     * @param request
//     * @param response
//     * @param handler
//     * @param ex
//     * @throws Exception
//     */
//    @Override
//    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
//        System.out.println("拦截器： 后处理执行。。。");
//    }
}
