package com.cm.service;

import com.cm.model.LinkModel;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface LinkService {

    //查询所有的link
    public List<LinkModel> selectLink();
}
