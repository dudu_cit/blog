package com.cm.service;

import com.cm.model.TagModel;

import java.util.List;

public interface TagService {

    //查询所有tag
    public List<TagModel> selectTag();
}
