package com.cm.service;

import com.cm.model.ArticleModel;
import com.cm.model.ArticleTypeModel;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ArticleService {

    //增加文章
    public void insertArticle(ArticleModel articleModel);

    //添加文章类型映射
    public void insertArticleType(ArticleTypeModel articleTypeModel);

    // 查询所有文章，集合以供 显示 和 模糊查询
    public List<ArticleModel> selectArticle();

    //根据文章的id查询文章的内容
    public ArticleModel selectArticleById(int article_id);

    //根据 文章的id 查询 文章的标签id
    public int selectArticleTypeId(int article_id);

    //根据 文章的id 查询 文章的标签id
    public String selectArticleType(int tag_id);

    //更新文章
    public void updateArticle(ArticleModel articleModel);

    //查询 当前用户的所有文章
    public List<ArticleModel> selectArticleByUserId(int article_user_id);

    //根据文章 id 删除文
    public void deleteArticleById(int article_id);

    //按 排名 查询 文章的 id
    public List<ArticleModel> getArticleOrder();

    //增加文章的点赞个数
    public int updateArticleLike(int article_like_count,int article_id);
}
