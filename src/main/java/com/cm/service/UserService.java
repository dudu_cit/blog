package com.cm.service;

import com.cm.model.UserEduModel;
import com.cm.model.UserInfoModel;
import com.cm.model.UserModel;
import org.apache.ibatis.annotations.Insert;

import java.util.List;

public interface UserService {

    // 查询所有用户账号信息
    public List<UserModel>  selectAllUserInfo();

    //根据用户名查询账号数据
    public UserModel selectUserByName(String username);

    //保存用户的账号数据
    public void insertUser(UserModel userModel);

    //更新用户账号数据
    public void updateUser(UserModel userModel);

    //更新用户信息
    public void updateUserBaseInfo( UserInfoModel userInfoModel);

    //查询用户信息
    public UserInfoModel selectUserBaseInfo(int id);

    //插入用户基本信息表
    public void insertUserBaseInfo(UserInfoModel userInfoModel);

    //查询用户教育信息
    public UserEduModel selectUserEduInfo(int id);

    //更新用户教育信息
    public void updateUserEduInfo(UserEduModel userEduModel);

    //插入用户教育信息
    public void insertUserEduInfo(UserEduModel userEduModel);

}
