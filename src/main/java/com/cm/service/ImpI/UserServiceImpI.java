package com.cm.service.ImpI;

import com.cm.dao.UserDao;
import com.cm.model.UserEduModel;
import com.cm.model.UserInfoModel;
import com.cm.model.UserModel;
import com.cm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户操作 业务层
 */
@Service("userService")

public class UserServiceImpI implements UserService {

    @Autowired
    private UserDao userDao;

    /**
     * 查询所有 用户账户信息
     * @return
     */
    @Override
    public List<UserModel> selectAllUserInfo() {
        return userDao.selectAllUserInfo();
    }

    /**
     * 根据用户信息查询 用户的账户信息
     * @param username
     * @return
     */
    @Override
    public UserModel selectUserByName(String username) {
        return userDao.selectUserByName(username);
    }


    /**
     * 保存用户的账户信息
     * @param userModel
     */
    @Override
    public void insertUser(UserModel userModel) {
        userDao.insertUser(userModel);
    }

    /**
     * 更新用户账户信息
     * @param userModel
     */
    @Override
    public void updateUser(UserModel userModel) {
        userDao.updateUser(userModel);
    }



    /**
     更新用户的基本信息
     * @param userInfoModel
     */
    @Override
    public void updateUserBaseInfo( UserInfoModel userInfoModel) {
        userDao.updateUserBaseInfo(userInfoModel);
    }

    /**
     * 查询用户基本信息
     * @param id
     * @return
     */
    @Override
    public UserInfoModel selectUserBaseInfo(int id) {
        return userDao.selectUserBaseInfo(id);
    }

    /**
     * 插入用户基本信息表
     * @param userInfoModel
     */
    @Override
    public void insertUserBaseInfo(UserInfoModel userInfoModel) {
        try {
            userDao.insertUserBaseInfo(userInfoModel);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * 查询用户教育信息
     * @param id
     * @return
     */
    @Override
    public UserEduModel selectUserEduInfo(int id) {
        return userDao.selectUserEduInfo(id);
    }

    /**
     * 更新用户教育信息
     * @param userEduModel
     */
    @Override
    public void updateUserEduInfo(UserEduModel userEduModel) {
        userDao.updateUserEduInfo(userEduModel);
    }

    /**
     * 插入用户教育信息
     * @param userEduModel
     */
    @Override
    public void insertUserEduInfo(UserEduModel userEduModel) {
        userDao.insertUserEduInfo(userEduModel);
    }
}
