package com.cm.service.ImpI;

import com.cm.dao.AuthorInfoDao;
import com.cm.model.AuthorInfoModel;
import com.cm.service.AuthorInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("AuthorInfoService")
public class AuthorInfoServiceImpI implements AuthorInfoService {

    @Autowired
    private AuthorInfoDao authorInfoDao;

    /**
     * 查询作者的基本信息：排名，博客数量，粉丝数量，访问量，点赞数量
     * @return
     */
    @Override
    public List<AuthorInfoModel> selectAuthorInfo() {
        return authorInfoDao.selectAuthorInfo();
    }

    /**
     * 查询粉丝的数量
     * @param id
     * @return
     */
    @Override
    public int SelectFaxNumber(int id) {
        return authorInfoDao.SelectFaxNumber(id);
    }
}
