package com.cm.service.ImpI;

import com.cm.dao.TagDao;
import com.cm.model.TagModel;
import com.cm.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("TagService")
public class TagServiceImpI implements TagService {

    @Autowired
    private TagDao tagDao;
    /**
     * 查询所有tag
     * @return
     */
    @Override
    public List<TagModel> selectTag() {

        return tagDao.selectTag();

    }
}
