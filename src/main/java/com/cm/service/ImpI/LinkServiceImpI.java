package com.cm.service.ImpI;

import com.cm.dao.LinkDao;
import com.cm.model.LinkModel;
import com.cm.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("LinkService")
public class LinkServiceImpI implements LinkService {

    @Autowired
    private LinkDao linkDao;

    /**
     * 查询所有的link
     * @return
     */
    @Override
    public List<LinkModel> selectLink() {
        return linkDao.selectLink();
    }
}
