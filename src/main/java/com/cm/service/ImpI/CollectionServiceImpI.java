package com.cm.service.ImpI;

import com.cm.dao.CollectionDao;
import com.cm.model.CollJiaModel;
import com.cm.model.CollectionModel;
import com.cm.service.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 收藏表
 */

@Service("CollectionService")
public class CollectionServiceImpI implements CollectionService {

    @Autowired
    private CollectionDao collectionDao;
    /**
     * 查询用户创建 的收藏夹
     * @param uid
     * @return
     */
    @Override
    public List<CollJiaModel> selectCJia(int uid) {
        return collectionDao.selectCJia(uid);
    }

    /**
     * 增加收藏夹
     * @param
     */
    @Override
    public void insertCJia(String a,int b) {
        System.out.println("collJiaModel11: "+a);
        collectionDao.insertCJia(a,b);
    }

    /**
     * 删除文件夹
     * @param CJia_id
     * @param CJia_user_id
     */
    @Override
    public int deleteCJia(int CJia_id, int CJia_user_id) {
        return collectionDao.deleteCJia(CJia_id,CJia_user_id);
    }

    /**
     * 查询 某个用户、某个收藏夹中中的内容
     * @param collection_user_id
     * @param collection_jia
     * @return
     */
    @Override
    public List<CollectionModel> selectCollection(int collection_user_id, int collection_jia) {
        return collectionDao.selectCollection(collection_user_id,collection_jia);
    }

    /**
     *
     * 取消收藏
     * @param collection_user_id
     * @param collection_article_id
     */
    @Override
    public int deleteCollection(int collection_user_id, int collection_article_id) {
        return collectionDao.deleteCollection(collection_user_id,collection_article_id);
    }

    /**
     * 查询某个用户、是否收藏过某个文章
     * @param collection_user_id
     * @param collection_article_id
     * @return
     */
    @Override
    public CollectionModel selectCollectionById(int collection_user_id, int collection_article_id) {
        return collectionDao.selectCollectionById(collection_user_id,collection_article_id);
    }

    /**
     * 添加 收藏 到 收藏夹
     * @param collection_user_id
     * @param collection_article_id
     * @param collection_jia
     * @return
     */
    @Override
    public int insertCollection(int collection_user_id, int collection_article_id, int collection_jia) {
        return collectionDao.insertCollection(collection_user_id,collection_article_id,collection_jia);
    }

    /**
     * 根据收藏夹、用户名删除 收藏，，，删除收藏夹中的所有内容
     * @param collection_user_id
     * @param collection_jia
     * @return
     */
    @Override
    public int deleteCollectionByCJIa(int collection_user_id, int collection_jia) {
        return collectionDao.deleteCollectionByCJIa(collection_user_id,collection_jia);
    }


}
