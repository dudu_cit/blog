package com.cm.service.ImpI;



import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;

public class MyAspectImpI {
    //前置通知
    public void myBefore(JoinPoint joinPoint){
        System.out.println("前置通知:"+joinPoint);
        System.out.println("被植入增强处理的目标方法:"+joinPoint.getSignature().getName());
    }
    //后置通知
    public void myAfterReturrning(JoinPoint joinPoint){
        System.out.println("后置通知:"+joinPoint);
        System.out.println("被植入增强处理的目标方法:"+joinPoint.getSignature().getName());
    }
    /*
     * 环绕通知
     * proceedingJoinPoint是JoinPoint子接口，表示可以执行目标方法
     * 1.必须是Object类型的返回值
     * 2.必须接收一个参数，类型为ProceedingJoinPoint
     * 3.必须throws Throwable
     */
    public Object myAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{
        System.out.println("环绕前通知");
        //执行当前目标方法
        Object obj = proceedingJoinPoint.proceed();
        System.out.println("环绕后通知");
        return obj;
    }
    //异常通知
    public void myAfterThrowing(JoinPoint joinPoint,Throwable e){
        System.out.println("异常通知:"+e.getMessage());
    }
    //最终通知
    public void myAfter(){
        System.out.println("最终通知");
    }
}
