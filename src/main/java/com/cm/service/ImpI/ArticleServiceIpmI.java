package com.cm.service.ImpI;

import com.cm.dao.ArticleDao;
import com.cm.model.ArticleModel;
import com.cm.model.ArticleTypeModel;
import com.cm.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("ArticleService")
public class ArticleServiceIpmI implements ArticleService {

    @Autowired
    private ArticleDao articleDao;

    /**
     * 增加文章
     * @param articleModel
     */
    @Override
    public void insertArticle(ArticleModel articleModel) {
        articleDao.insertArticle(articleModel);
    }

    /**
     * 添加文章类型映射
     * @param articleTypeModel
     */
    @Override
    public void insertArticleType(ArticleTypeModel articleTypeModel) {
        articleDao.insertArticleType(articleTypeModel);
    }

    /**
     * 查询所有文章，集合以供 显示 和 模糊查询
     * @return
     */
    @Override
    public List<ArticleModel> selectArticle() {
        return articleDao.selectArticle();
    }

    /**
     * 根据文章的id查询文章的内容
     * @param article_id
     * @return
     */
    @Override
    public ArticleModel selectArticleById(int article_id) {
        return articleDao.selectArticleById(article_id);
    }

    /**
     * 根据 文章的id 查询 文章的标签id
     * @param article_id
     * @return
     */
    @Override
    public int selectArticleTypeId(int article_id) {
        return articleDao.selectArticleTypeId(article_id);
    }

    /**
     * 根据 文章的id 查询 文章的标签id
     * @param tag_id
     * @return
     */
    @Override
    public String selectArticleType(int tag_id) {
        return articleDao.selectArticleType(tag_id);
    }

    /**
     * 更新文章
     * @param articleModel
     */
    @Override
    public void updateArticle(ArticleModel articleModel) {
        articleDao.updateArticle(articleModel);
    }

    /**
     * 查询 当前用户的所有文章
     * @param article_user_id
     * @return
     */
    @Override
    public List<ArticleModel> selectArticleByUserId(int article_user_id) {
        return articleDao.selectArticleByUserId(article_user_id);
    }

    /**
     * 根据文章 id 删除文
     * @param article_id
     */
    @Override
    public void deleteArticleById(int article_id) {
        articleDao.deleteArticleById(article_id);
    }

    /**
     * 按 排名 查询 文章的 id
     * @return
     */
    @Override
    public List<ArticleModel> getArticleOrder() {
        return articleDao.getArticleOrder();
    }

    /**
     * 增加文章的点赞个数
     * @param article_like_count
     * @param article_id
     * @return
     */
    @Override
    public int updateArticleLike(int article_like_count, int article_id) {
        return articleDao.updateArticleLike(article_like_count,article_id);
    }


}
