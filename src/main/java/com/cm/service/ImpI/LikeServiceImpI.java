package com.cm.service.ImpI;

import com.cm.dao.LikeDao;
import com.cm.model.LikeModel;
import com.cm.service.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("LikeService")
public class LikeServiceImpI implements LikeService {

    @Autowired
    private LikeDao likeDao;

    /**
     * 查询 某个用户是否为 某个文章点过赞
     * @param like_user_id
     * @param like_article_id
     * @return
     */
    @Override
    public LikeModel selectLikeById(int like_user_id, int like_article_id) {
        return likeDao.selectLikeById(like_user_id,like_article_id);
    }

    /**
     * 取消 点赞
     * @param like_user_id
     * @param like_article_id
     * @return
     */
    @Override
    public int deleteLike(int like_user_id, int like_article_id) {
        return likeDao.deleteLike(like_user_id,like_article_id);
    }

    /**
     * 点赞 增加一条数据
     * @param like_user_id
     * @param like_article_id
     * @return
     */
    @Override
    public int insertLike(int like_user_id, int like_article_id) {
        return likeDao.insertLike(like_user_id,like_article_id);
    }
}
