package com.cm.service;

import com.cm.dao.AuthorInfoDao;
import com.cm.model.AuthorInfoModel;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface AuthorInfoService {

    //查询作者的基本信息：排名，博客数量，粉丝数量，访问量，点赞数量
    public List<AuthorInfoModel> selectAuthorInfo();

    //查询粉丝的数量
    public int SelectFaxNumber(int id);
}
