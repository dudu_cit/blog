package com.cm.service;

import com.cm.model.CollJiaModel;
import com.cm.model.CollectionModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CollectionService {

    //查询用户创建 的收藏夹
    public List<CollJiaModel> selectCJia(int uid);

    //增加收藏夹
    public void insertCJia(String a,int b);

    //删除文件夹
    public int deleteCJia(int CJia_id,int CJia_user_id);

    //查询 某个用户、某个收藏夹中中的内容
    public List<CollectionModel> selectCollection(int collection_user_id,int collection_jia);

    //取消收藏
    public int deleteCollection(int collection_user_id,int collection_article_id);

    //查询某个用户、
    public CollectionModel selectCollectionById(int collection_user_id,int collection_article_id);

    //添加 收藏 到 收藏夹
    public int insertCollection(int collection_user_id,int collection_article_id,int collection_jia);

    //根据收藏夹、用户名删除 收藏，，，删除收藏夹中的所有内容
    public int deleteCollectionByCJIa(int collection_user_id,int collection_jia);
}
