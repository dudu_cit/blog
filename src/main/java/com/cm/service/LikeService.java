package com.cm.service;

import com.cm.model.LikeModel;

public interface LikeService {

    // 查询 某个用户是否为 某个文章点过赞
    public LikeModel selectLikeById(int like_user_id,int like_article_id);

    //取消 点赞
    public int deleteLike(int like_user_id,int like_article_id);

    //点赞 增加一条数据
    public int insertLike(int like_user_id,int like_article_id);
}
