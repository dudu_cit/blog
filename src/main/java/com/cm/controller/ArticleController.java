package com.cm.controller;

import com.cm.model.*;
import com.cm.service.ArticleService;
import com.cm.service.AuthorInfoService;
import com.cm.service.CollectionService;
import com.cm.service.LikeService;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 * 文章管理类 controller 处理器，
 */
@Controller
@RequestMapping("/article")
@SessionAttributes(value = {"session"})
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private AuthorInfoService authorInfoService;

    @Autowired
    private CollectionService collectionService;

    @Autowired
    private LikeService likeService;

    /**
     * 上传文章成功后，跳转页面 ，带有参数，解析参数
     * @return
     */
    @RequestMapping("/jumpPage")
    public String jumpPage(){
        return "/jumpPage";
    }


    /**
     * 跳转 到指定文章的 内容页面
     * 加载文章信息
     * @param request
     * @return
     */
    @RequestMapping("/blog_content")
    public String blog_content(HttpServletRequest request, HashMap<String, Object> hashMap, ModelMap modelMap){
        try{
            //得到转载文章的 id
            String id = request.getParameter("aid");
            //如果得到的 id 为空，跳转到错误页面
            if(id == null) return "error";
            int aid = Integer.parseInt(id);
            //System.out.println("aid : "+aid);

            /** 查询 文章基本信息 **/
            //查询文章数据
            ArticleModel articleModel = articleService.selectArticleById(aid);
            //访问数 +1
            articleModel.setArticle_view_count(articleModel.getArticle_view_count()+1);
            //更新文章
            articleService.updateArticle(articleModel);
            //查询 文章的 标签id
            int tag_id = articleService.selectArticleTypeId(articleModel.getArticle_id());
            //根据 标签id 查询 标签的名字
            String tag_name = articleService.selectArticleType(tag_id);
            articleModel.setArticle_type_name(tag_name);
            //查询 文章 类型
            if(articleModel.getArticle_status() == 1){//原创
                articleModel.setArticle_status_name("原创");
            }else{//转载
                articleModel.setArticle_status_name("转载");
            }
            //设置hashMap
            hashMap.put("articleModel_content",articleModel);
            hashMap.put("articleModel_tag_name",tag_name);

            /** 查询作者 基本信息 **/
            //System.out.println("author_id:  "+articleModel.getArticle_user_id());
            List<AuthorInfoModel> authorInfoModel = authorInfoService.selectAuthorInfo();
            int fax_number = authorInfoService.SelectFaxNumber(articleModel.getArticle_user_id());
            int m = 0;
            //找到用户的id
            for(AuthorInfoModel authorInfoModels:authorInfoModel){ m++;if(authorInfoModels.getAuthor_id() == articleModel.getArticle_user_id()){ m = m-1;break; } } ;
            //设置粉丝数量
            authorInfoModel.get(m).setFax_number(fax_number);
            //计算 排名
            authorInfoModel.get(m).setAuthor_order(m+1);
            //设置hashMap，显示在页面中
            hashMap.put("authorInfoModel",authorInfoModel.get(m));

            /** 查询该文章是否被用户 收藏过或者点赞过 **/
            //用户id
            UserModel userModel = (UserModel) modelMap.get("session");
            int collection_user_id = userModel.getId();
            //文章id
            int collection_article_id = articleModel.getArticle_id();
            CollectionModel collectionModel = collectionService.selectCollectionById(collection_user_id,collection_article_id);
            if(collectionModel == null){//没有收藏
                hashMap.put("isCollect",0);
            }else{//收藏过
                hashMap.put("isCollect",1);
            }

            /** 查询该文章是否被用户点赞过 **/
            LikeModel likeModel = likeService.selectLikeById(collection_user_id,collection_article_id);
            if(likeModel == null){//没有收藏
                hashMap.put("isLike",0);
            }else{//收藏过
                hashMap.put("isLike",1);
            }



        }catch (Exception e){
            e.printStackTrace();
        }
        return "/blog_content";

    }

    /**
     * 更新文章
     * @param articleModel 需要更新的数据
     * @return
     */
    @RequestMapping("/updateArticle")
    public String updateArticle(ArticleModel articleModel){

        return "/";
    }
    /**
     * 上传文章
     * @param articleModel
     * @return
     * @throws ServletException
     * @throws IOException
     * @throws JSONException
     */
    @RequestMapping("/uploadArticle")
    @ResponseBody
    public Map<String, Object> uploadArticle(@RequestBody ArticleModel articleModel) throws ServletException, IOException, JSONException {
        //设置返回信息
        Map<String, Object> map = new HashMap<String, Object>();
        System.out.println("articleModel: "+articleModel);
        String html = articleModel.getArticle_content();
        String str = html.replaceAll("\\<.*?\\>","");
        str = StringEscapeUtils.unescapeHtml(str);
        System.out.println(" str :" +str);
        articleModel.setArticle_summary(str);
        try{
            //添加文章表
            articleService.insertArticle(articleModel);
            System.out.println("new article id is : "+articleModel.getArticle_id());
            //添加映射表
            ArticleTypeModel articleTypeModel = new ArticleTypeModel();
            articleTypeModel.setArticle_id(articleModel.getArticle_id());
            articleTypeModel.setArticle_type_id(articleModel.getArticle_type_id());
            articleService.insertArticleType(articleTypeModel);
            map.put("code",articleModel.getArticle_id());
        }catch (Exception e){
            map.put("code",0);
            e.printStackTrace();
        }
        return map;
    }


    /**
     * 将 收藏夹得信息显示在 弹出窗口
     * @return
     */
    @RequestMapping("/CJia_add")
    public String CJia_add(HttpServletRequest request,HashMap<String, Object> hashMap, ModelMap modelMap){
        //得到转载文章的 id
        String id = request.getParameter("aid");
        //如果得到的 id 为空，跳转到错误页面
        //if(id == null) return "error";
        int aid = Integer.parseInt(id);
        //System.out.println("ai7d : "+aid);
        //加载收藏夹 ， 得到收藏夹 列表
        UserModel userModel = (UserModel) modelMap.get("session");
        List<CollJiaModel> Clist = collectionService.selectCJia(userModel.getId());
        List<CollJiaModel> list = Clist;
        //将 收藏夹列表 和 文章 id 传递给 弹出窗口
        hashMap.put("CJia",list);
        hashMap.put("aid",aid);
        return "/Popup/CJIa_add";
    }
}
