package com.cm.controller;


import com.cm.model.CollJiaModel;
import com.cm.model.CollectionModel;
import com.cm.model.UserModel;
import com.cm.service.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/user")
@SessionAttributes(value = {"session"})
public class CollectionController {

    @Autowired
    private CollectionService collectionService;

    @RequestMapping("/collection")
    public String collection(HttpServletRequest request,HttpServletResponse response, ModelMap modelMap, HashMap<String, Object> hashMap) throws IOException {
        int CJid = 1;
        String CJIa_id = request.getParameter("CJia_id");

        //默认是 默认收藏夹，CJIa_id != null,说明用户点击其他的收藏夹
        if(CJIa_id != null){
            CJid = Integer.parseInt(CJIa_id);
            //System.out.println("CJIa_id : "+CJIa_id);
        }
        UserModel userModel = (UserModel) modelMap.get("session");
        if(userModel == null){ //用户没有登陆
            response.sendRedirect("/user/login");
        }else{//用户登陆
            /** 初始化 收藏夹 **/
            List<CollJiaModel> Clist = collectionService.selectCJia(userModel.getId());
            List<CollJiaModel> list = Clist;
            hashMap.put("CJia",list);

            /** 初始化 默认收藏夹中的内容 '1' 表示默认收藏夹 **/

            List<CollectionModel> collectionModels = collectionService.selectCollection(userModel.getId(),CJid);
            //System.out.println("collectionModels : "+collectionModels);
            hashMap.put("collectionModels",collectionModels);
            // 当前显示收藏夹的ID
            hashMap.put("CJia_id",CJid);
      }
        return "/collection";
    }

    //collection/addJia
    /**
     * 增加收藏夹
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/addJia")
    @ResponseBody
    public Map<String, Object> addJia(String CJia_name,String CJia_user_id) throws ServletException, IOException {
        //设置返回信息
        Map<String, Object> map = new HashMap<String, Object>();
        CollJiaModel collJiaModel = new CollJiaModel();
        //System.out.println("CJia_name ： " +CJia_name);
        int uid  = Integer.parseInt(CJia_user_id);

        collJiaModel.setCJia_name("new");
        collJiaModel.setCjia_user_id(1);
        //System.out.println("collJiaModel : "+collJiaModel);
        //查询数据
        collectionService.insertCJia(CJia_name,uid);
        map.put("code",0);
        return map;
    }


    /**
     * 删除收藏夹
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/deleteJia")
    @ResponseBody
    public Map<String, Object> deleteJia(String CJia_id,String CJia_user_id) throws ServletException, IOException {
        //设置返回信息
        Map<String, Object> map = new HashMap<String, Object>();
        int cid = Integer.parseInt(CJia_id);
        int cuid = Integer.parseInt(CJia_user_id);
        //删除 收藏夹
        int reDeleteCJia = collectionService.deleteCJia(cid,cuid);
        if( reDeleteCJia == 1){
            int reDeleteCollectionByCJIa =  collectionService.deleteCollectionByCJIa(cuid,cid);
            if(reDeleteCollectionByCJIa == 1){
                map.put("code",1);
            }
        }
        return map;
    }

    /**
     * 取消收藏
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/CancelCollect")
    @ResponseBody
    public Map<String, Object> CancelCollect(String collection_user_id,String collection_article_id) throws ServletException, IOException {
        //设置返回信息
        Map<String, Object> map = new HashMap<String, Object>();
        int cuid = Integer.parseInt(collection_user_id);
        int caid = Integer.parseInt(collection_article_id);
        //System.out.println("collection_user_id: "+cuid);
        //System.out.println("collection_article_id : "+caid);
        //删除数据
        int reDeleteCollect= collectionService.deleteCollection(cuid,caid);
        if(reDeleteCollect == 1){
            map.put("code",1 );
        }
        return map;
    }


    @RequestMapping("/addCollection")
    @ResponseBody
    public Map<String, Object> addCollection(String collection_user_id,String collection_article_id,String collection_jia){
        //设置返回信息
        Map<String, Object> map = new HashMap<String, Object>();
        int id  = Integer.parseInt(collection_user_id);
        int aid = Integer.parseInt(collection_article_id);
        int cid = Integer.parseInt(collection_jia);
        //System.out.println("id: "+id);
        // System.out.println("aid: "+aid);
        //System.out.println("cid: "+cid);
        int reInsertCollection = collectionService.insertCollection(id,aid,cid);
        if(reInsertCollection == 1){
            map.put("code",1 );
        }
        return map;
    }



}
