package com.cm.controller;


import com.cm.dao.TagDao;
import com.cm.model.ArticleModel;
import com.cm.model.AuthorInfoModel;
import com.cm.model.TagModel;
import com.cm.model.UserModel;
import com.cm.service.ArticleService;
import com.cm.service.AuthorInfoService;
import com.cm.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * 我的博客界面 controller
 */
@Controller
@SessionAttributes(value = {"session"})
public class MyBlogController {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private AuthorInfoService authorInfoService;

    @Autowired
    private TagService tagService;

    /**
     * 初始化用户，的博客界面
     * @return
     */
    @RequestMapping("/my_blog")
    public String my_blog(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap, HashMap<String, Object> hashMap) throws IOException {
        //得到用户的 id
        //得到用户账号session信息
        UserModel userModel = (UserModel) modelMap.get("session");
        if(userModel == null){
            response.sendRedirect("/user/login");
        }else {
            /** 查询博客文章 **/
            List<ArticleModel> list = articleService.selectArticleByUserId(userModel.getId());
            //加载每个博客的类型和tag
            for(ArticleModel lists:list){
                //查询 文章的 标签id
                int tag_id = articleService.selectArticleTypeId(lists.getArticle_id());
                //根据 标签id 查询 标签的名字
                String tag_name = articleService.selectArticleType(tag_id);
                lists.setArticle_type_name(tag_name);
                //查询 文章 类型
                if(lists.getArticle_status() == 1){//原创
                    lists.setArticle_status_name("原创");
                }else{//转载
                    lists.setArticle_status_name("转载");
                }
            }
            hashMap.put("articleModel_list",list);
            /** 查询作者基本信息 **/
//            AuthorInfoModel authorInfoModel = authorInfoService.selectAuthorInfo(userModel.getId());
//            int fax_number = authorInfoService.SelectFaxNumber(userModel.getId());
//            authorInfoModel.setFax_number(fax_number);
            List<AuthorInfoModel> authorInfoModel = authorInfoService.selectAuthorInfo();
            int fax_number = authorInfoService.SelectFaxNumber(userModel.getId());
            int m = 0;
            //找到用户的id
            for(AuthorInfoModel authorInfoModels:authorInfoModel){ m++;if(authorInfoModels.getAuthor_id() == userModel.getId()){ m = m-1;break; } } System.out.println("m="+m);
            //设置粉丝数量
            authorInfoModel.get(m).setFax_number(fax_number);
            //计算 排名
            authorInfoModel.get(m).setAuthor_order(m+1);
            //设置hashMap，显示在页面中
            hashMap.put("authorInfoModel",authorInfoModel.get(m));
            System.out.println("authorInfoModel : "+authorInfoModel);
            /** 显示 标签云 **/
            List<TagModel> tagModel = tagService.selectTag();
            if(tagModel == null){
                System.out.println("tagModel is null ");
            }else{
                hashMap.put("tagModel",tagModel);
                System.out.println("tagModel: "+tagModel);
            }


        }
        return "/my-blog";
    }


    /**
     * 删除 博客
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping("/deleteArticle")
    public void deleteArticle(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String article_id = request.getParameter("article_id");
        int id = Integer.parseInt(article_id);
        System.out.println("article_id : "+article_id);
        articleService.deleteArticleById(id);
        response.sendRedirect("/my_blog");
    }

}
