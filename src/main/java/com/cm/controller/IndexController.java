package com.cm.controller;


import com.cm.model.*;
import com.cm.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;


/**
 * 主界面 controller
 */
@Controller
@RequestMapping("/admin")
public class IndexController {

    @Autowired
    private UserService userService;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private AuthorInfoService authorInfoService;

    @Autowired
    private TagService tagService;

    @Autowired
    private LinkService linkService;



    /**
     * 显示 博客 主页
     * @return
     */
    @RequestMapping("/index")
    public String index(HashMap<String, Object> hashMap){

        /** 显示 首推博客，博客列表,排序按 访问量排序 **/
        List<ArticleModel> list = articleService.getArticleOrder();
        //加载每个博客的类型和tag
        for(ArticleModel lists:list){
            //查询 文章的 标签id
            int tag_id = articleService.selectArticleTypeId(lists.getArticle_id());
            //根据 标签id 查询 标签的名字
            String tag_name = articleService.selectArticleType(tag_id);
            lists.setArticle_type_name(tag_name);
            //查询 文章 类型
            if(lists.getArticle_status() == 1){//原创
                lists.setArticle_status_name("原创");
            }else{//转载
                lists.setArticle_status_name("转载");
            }
        }
        /** 显示 作者排名 **/
        List<AuthorInfoModel> authorInfoModel = authorInfoService.selectAuthorInfo();

        /** 设置hashMap **/
        hashMap.put("AllBlog",list);
        hashMap.put("authorInfoModel",authorInfoModel);
        /** 显示 站长推荐 **/
        List<ArticleModel> list1 = articleService.selectArticle();
        hashMap.put("authorInfoModel2",list1);

        /** 显示 标签云 **/
        List<TagModel> tagModel = tagService.selectTag();
        if(tagModel == null){
            System.out.println("tagModel is null ");
        }else{
            hashMap.put("tagModel",tagModel);
            //System.out.println("tagModel: "+tagModel);
        }
        /** 显示友情链接 **/
        List<LinkModel> list2 = linkService.selectLink();
        System.out.println("list2 : "+list2);
        hashMap.put("linkModel",list2);

        return "/index";
    }

}
