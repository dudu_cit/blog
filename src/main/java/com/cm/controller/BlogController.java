package com.cm.controller;

import com.cm.model.ArticleModel;
import com.cm.model.TagModel;
import com.cm.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;

/**
 * 博客界面 controller
 */
@Controller
@RequestMapping("/admin")
public class BlogController {


    @Autowired
    private UserService userService;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private AuthorInfoService authorInfoService;

    @Autowired
    private TagService tagService;

    @Autowired
    private LinkService linkService;

    /**
     * 博客界面
     * @return
     */
    @RequestMapping("blog")
    public String blog(HashMap<String, Object> hashMap){

        /** 显示 博客列表 按时间顺序 默认显示 ： 所有类型文章 。按时间顺序 **/
        List<ArticleModel> list1 = articleService.selectArticle();
        //加载每个博客的类型和tag
        for(ArticleModel lists:list1){
            //查询 文章的 标签id
            int tag_id = articleService.selectArticleTypeId(lists.getArticle_id());
            //根据 标签id 查询 标签的名字
            String tag_name = articleService.selectArticleType(tag_id);
            lists.setArticle_type_name(tag_name);
            //查询 文章 类型
            if(lists.getArticle_status() == 1){//原创
                lists.setArticle_status_name("原创");
            }else{//转载
                lists.setArticle_status_name("转载");
            }
        }
        hashMap.put("authorInfoModel1",list1);

        /** 显示 标签云 **/
        List<TagModel> tagModel = tagService.selectTag();
        hashMap.put("tagModel",tagModel);

        /** 显示 热门博客 **/
        List<ArticleModel> list2 = articleService.getArticleOrder();
        hashMap.put("authorInfoModel2",list2);
        return "/blog";
    }
}
