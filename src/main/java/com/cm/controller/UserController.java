package com.cm.controller;

import com.cm.model.*;
import com.cm.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/user")
//设置登陆界面的 session
@SessionAttributes(value = {"session"})
public class UserController {

    @Autowired
    private UserService userService;



    @RequestMapping("info")
    public String info(HttpServletResponse response, ModelMap modelMap) throws IOException {
        UserModel userModel = (UserModel) modelMap.get("session");
        if(userModel == null){
            response.sendRedirect("/user/login");
        }return "/info"; }

    @RequestMapping("messages")
    public String messages(HttpServletResponse response, ModelMap modelMap) throws IOException {
        UserModel userModel = (UserModel) modelMap.get("session");
        if(userModel == null){
            response.sendRedirect("/user/login");
        }
        return "/messages"; }

    @RequestMapping("markdown")
    public String markdown(HttpServletResponse response, ModelMap modelMap) throws IOException {
        UserModel userModel = (UserModel) modelMap.get("session");
        if(userModel == null){
            response.sendRedirect("/user/login");
        }
        return "/markdown"; }

    @RequestMapping("login")
    public String login(){ return "/login"; }

    @RequestMapping("error")
    public String error(){ return "/error"; }

    @RequestMapping("userSet")
    public String userSet(HttpServletResponse response, ModelMap modelMap) throws IOException {
        UserModel userModel = (UserModel) modelMap.get("session");
        if(userModel == null){
            response.sendRedirect("/user/login");
        }
        return "/userSet"; }

    @RequestMapping("userPage")
    public String userPage(HttpServletResponse response, ModelMap modelMap) throws IOException {
        UserModel userModel = (UserModel) modelMap.get("session");
        if(userModel == null){
            response.sendRedirect("/user/login");
        }
        return "/userPage"; }

    @RequestMapping("registration")
    public String registration(){ return "/registration"; }



    /**
     * 给用户信息界面加载用户基本信息
     * @return
     */
    @RequestMapping("/showUserBaseInfo")
    public String showUserBaseInfo(ModelMap modelMap, HashMap<String, Object> hashMap){
        //得到用户账号session信息
        UserModel userModel = (UserModel) modelMap.get("session");
        //得到登陆账号的id
        System.out.println("userModel : "+userModel);
        UserInfoModel userInfoModel = userService.selectUserBaseInfo(userModel.getId());
        UserEduModel userEduModel = userService.selectUserEduInfo(userModel.getId());
        System.out.println("userInfoModel : " + userInfoModel);
        System.out.println("userEduModel : "+userEduModel );
        //将数据存储到 model 中
        hashMap.put("userInfo_session",userInfoModel);
        hashMap.put("userEdu_session",userEduModel);
        return "/userPage";
    }

    /**
     * 登陆验证操作
     * @param userModel
     * @param model
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/userLogin")
    @ResponseBody
    public Map<String, Object> userLogin(@RequestBody UserModel userModel, Model model) throws ServletException, IOException {
        //设置返回信息
        Map<String, Object> map = new HashMap<String, Object>();

        System.out.println("用户名 ： " + userModel.getUsername());
        System.out.println("密码 ： " + userModel.getPassword());

        //登陆验证
        UserModel userModel1 = userService.selectUserByName(userModel.getUsername());
        System.out.println(userModel1);
        if(userModel1 == null){
            //登陆失败
            map.put("code",0);
            map.put("msg","该用户尚未注册");
        }else if( !userModel1.getPassword().equals(userModel.getPassword())){
            //登陆成功
            map.put("code",0);
            map.put("msg","密码错误");
        }else{
            //设置 session
            UserModel userModels = userModel1;
            model.addAttribute("session",userModels);
            //登陆成功
            map.put("code",1);
            map.put("msg","");
        }
        return map;
    }

    /**
     * 退出登陆
     * @return
     */
    @RequestMapping("/outLogin")

    public String  outLogin(SessionStatus status,HashMap<String, Object> hashMap) throws IOException {
        //注销 session
        status.setComplete();
        //注销 hashmap
        hashMap.remove("userInfo_session");
        hashMap.remove("userEdu_session");
        System.out.println("注销 session和hashmap");
        return "/index";
    }


    /**
     * 验证账号是否已经注册过了
     * @param username
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/provingUsername")
    @ResponseBody
    public Map<String, Object> provingUsername(@RequestParam("username") String username) throws ServletException, IOException {
        //设置返回信息
        Map<String, Object> map = new HashMap<String, Object>();
        System.out.println("用户名 ： " + username);
        //查询数据
        UserModel userModel1 = userService.selectUserByName(username);
        if(userModel1 == null){
            //登陆失败
            map.put("code",0);
        }else {
            //该用户已经注册过了
            map.put("code",1);
        }
        return map;
    }



    /**
     * 用户注册
     * @param userModel
     * @return
     */
    @RequestMapping("/userRigistrate")
    @ResponseBody
    public String userRigistrate(@RequestBody UserModel userModel){
        System.out.println(userModel);
        userService.insertUser(userModel);
        //得到刚刚注册用户信息的 id
        UserModel userModel1 = userService.selectUserByName(userModel.getUsername());
        //
        UserInfoModel userInfoModel = new UserInfoModel();
        UserEduModel userEduModel = new UserEduModel();
        //创建关联表的信息
        userInfoModel.setUid(userModel1.getId());
        userEduModel.setUid(userModel1.getId());

        //添加关联表
        userService.insertUserBaseInfo(userInfoModel);
        userService.insertUserEduInfo(userEduModel);

        System.out.println("userInfoModel"+userInfoModel);
        System.out.println("userEduModel"+userEduModel);
        //
        return "ok";
    }


    /**
     * 修改用户基本信息
     * @param userInfoModel
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/updateUserBaseInfo")
    @ResponseBody
    public Map<String, Object> updateUserBaseInfo(@RequestBody UserInfoModel userInfoModel) throws ServletException, IOException {
        //设置返回信息
        Map<String, Object> map = new HashMap<String, Object>();
        System.out.println("userInfoModel : "+userInfoModel);
        //更新数据
        try{
            userService.updateUserBaseInfo(userInfoModel);
            map.put("code",1);
        }catch (Exception e){
            map.put("code",0);
            e.printStackTrace();
        }
        return map;
    }


    /**
     * 修改用户教育信息
     * @param userEduModel
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/updateUserEduInfo")
    @ResponseBody
    public Map<String, Object> updateUserEduInfo(@RequestBody UserEduModel userEduModel) throws ServletException, IOException {
        //设置返回信息
        Map<String, Object> map = new HashMap<String, Object>();
        System.out.println("userEduModel : "+userEduModel);
        //更新数据
        try{
            userService.updateUserEduInfo(userEduModel);
            map.put("code",1);
        }catch (Exception e){
            map.put("code",0);
            e.printStackTrace();
        }
        return map;
    }

    /**
     * 修改用户账户信息
     * @param UserModel
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/updateUser")
    @ResponseBody
    public Map<String, Object> updateUser(@RequestBody UserModel UserModel) throws ServletException, IOException {
        //设置返回信息
        Map<String, Object> map = new HashMap<String, Object>();
        System.out.println("UserModel : "+UserModel);
        //更新数据
        try{
            userService.updateUser(UserModel);
            map.put("code",1);
        }catch (Exception e){
            map.put("code",0);
            e.printStackTrace();
        }
        return map;
    }

    @RequestMapping("/reloadSession")
    public String  reloadSession(ModelMap modelMap, Model model,SessionStatus status){
        //得到用户账号session信息
        UserModel userModel = (UserModel) modelMap.get("session");
        UserModel userModel1 = userService.selectUserByName(userModel.getUsername());
        //设置新的session
        System.out.println("userModel1: "+userModel1);
        model.addAttribute("session",userModel1);
        //model.addAttribute("session",userModels);
        return "/userSet";
    }

}
