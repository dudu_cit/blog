package com.cm.controller;

import com.cm.service.ArticleService;
import com.cm.service.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/article")
@SessionAttributes(value = {"session"})
public class LikeController {

    @Autowired
    private LikeService likeService;

    @Autowired
    private ArticleService articleService;

    @RequestMapping("/addLike")
    @ResponseBody
    public Map<String, Object>  addLike(String like_user_id,String like_article_id,String article_like_count){
        Map<String, Object> map = new HashMap<String, Object>();
        int uid = Integer.parseInt(like_user_id);
        int aid = Integer.parseInt(like_article_id);
        int likeCount = Integer.parseInt(article_like_count)+1;
        //给点赞表添加数据
        int reLike = likeService.insertLike(uid,aid);
        //给文章的点赞数 + 1
        if(reLike == 1){//如果 insertLike 操作失败，不执行addArticleLike
            int reAddLike = articleService.updateArticleLike(likeCount,aid);
            if(reAddLike ==1) {
                map.put("code",1);
            }
        }
        return map;
    }

    /**
     * 取消 点赞
     * @param like_user_id
     * @param like_article_id
     * @param article_like_count
     * @return
     */
    @RequestMapping("/cancelLike")
    @ResponseBody
    public Map<String, Object>  cancelLike(String like_user_id,String like_article_id,String article_like_count){
        Map<String, Object> map = new HashMap<String, Object>();
        int uid = Integer.parseInt(like_user_id);
        int aid = Integer.parseInt(like_article_id);
        int likeCount = Integer.parseInt(article_like_count)-1;
        //删除 点赞信息
        //给点赞表添加数据
        int reLike = likeService.deleteLike(uid,aid);
        //给文章的点赞数 + 1
        if(reLike == 1){//如果 insertLike 操作失败，不执行addArticleLike
            int reAddLike = articleService.updateArticleLike(likeCount,aid);
            if(reAddLike ==1) {
                map.put("code",1);
            }
        }
        return map;
    }
}
