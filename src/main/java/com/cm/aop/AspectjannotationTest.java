package com.cm.aop;

import com.cm.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AspectjannotationTest {
    @Test
    public void run1(){

        ApplicationContext ac = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        UserDao userDao = (UserDao) ac.getBean("UserDao");
        userDao.addUser();
        System.out.println("--------分割线--------");
        //userDao.delUser();

    }

}
